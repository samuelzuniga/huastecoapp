// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const version = 1;
const stage = 'dev';
export const environment = {
	production: false,
	servicesURL: `https://0pbqidlac5.execute-api.us-east-1.amazonaws.com/${stage}/api/v${version}`,
	huastecoAPIKey: 'wcSGXEzcPt2SIVXo0o6zd8dK6d7q2YXK62ucXABC',
	googleAPIKey: 'AIzaSyAc77mHQsCkUF1oUR7-amC07yr7yIVdfjo',
	uploadBucket: 'https://s3.amazonaws.com/huasteco-upload-bucket',
	defaultUserImage: 'user.png',
	headers : {
		huastecoIgnoreHttpInterceptors: 'huasteco-ignore-http-interceptors'
	}
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
