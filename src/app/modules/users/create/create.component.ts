import {
	Component,
	OnInit
} from '@angular/core';
import {
	FormBuilder,
	FormGroup,
	Validators
} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';

import {validateFormControls} from '../../shared/helpers/input-validation';
import {UsersService} from '../../../root-services/api/users.service';
import {IGroup, IUser} from '../../../../../server/src-shared/interfaces';
import {IClientError} from '../../shared/interfaces/client-error.interface';
import {INTERNATIONAL_PHONE_NUMBER_PATTERN} from '../../shared/helpers/patterns';
import {FlashMessageService} from '../../core/services/flash-message.service';

@Component({
	selector: 'app-huasteco-users-create',
	templateUrl: './create.component.html',
	styleUrls: ['./create.component.scss']
})
export class UsersCreateComponent implements OnInit {
	createUserForm: FormGroup;
	controlHasError: Function;
	groups: IGroup[] = []; // receive it as an input
	isSubmitting: boolean;

	constructor(
		private flashMessageService: FlashMessageService,
		private route: ActivatedRoute,
		private formBuilder: FormBuilder,
		private usersService: UsersService) {
	}

	ngOnInit() {
		this.isSubmitting = false;
		this.groups = this.route.snapshot.data.groups;

		this.createUserForm = this.formBuilder.group({
			username: ['', [Validators.required]],
			firstName: ['', [Validators.required]],
			group: ['', [Validators.required]],
			lastName: ['', [Validators.required]],
			phoneNumber: ['', [Validators.required, Validators.pattern(INTERNATIONAL_PHONE_NUMBER_PATTERN)]],
			email: ['', [Validators.required, Validators.email]]
		});

		this.controlHasError = validateFormControls(this.createUserForm);
	}

	onSubmit() {
		this.isSubmitting = true;
		this.usersService.createUser(this.createUserForm.value)
			.subscribe((user: IUser) => {
				this.isSubmitting = false;
				this.flashMessageService.showSuccess(`Usuario ${user.username} creado exitosamente`);
			}, (error: IClientError) => {
				this.isSubmitting = false;
				this.flashMessageService.showError(error.message);
			});
	}
}
