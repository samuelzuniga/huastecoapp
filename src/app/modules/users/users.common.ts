import { Routes } from '@angular/router';

// components
import {UsersHomeComponent} from './users-home/users-home.component';
import {UsersCreateComponent} from './create/create.component';

// services
import {MessageService} from './services/message.service';

import {GetGroupsResolver} from '../../root-services/resolvers/get-groups.resolver.';

export const COMPONENT_DECLARATIONS: any[] = [
	UsersHomeComponent,
	UsersCreateComponent
];

export const PROVIDERS_DECLARATIONS: any[] = [
	MessageService
];

export const ROUTES: Routes = [
	{
		path: '',
		component: UsersHomeComponent,
		children: [
			{
				path: 'create',
				component: UsersCreateComponent,
				resolve: {
					groups: GetGroupsResolver
				}
			}
		]
	}
];
