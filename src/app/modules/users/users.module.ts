import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';

// modules
import {MaterialModule} from '../material/material.module';
import {UsersRoutingModule} from './users-routing.module';
import {SharedModule} from '../shared/shared.module';

import {COMPONENT_DECLARATIONS, PROVIDERS_DECLARATIONS} from './users.common';

@NgModule({
	imports: [
		CommonModule,
		FlexLayoutModule,
		FormsModule,
		ReactiveFormsModule,
		UsersRoutingModule,
		MaterialModule,
		SharedModule
	],
	providers: PROVIDERS_DECLARATIONS,
	declarations: COMPONENT_DECLARATIONS,
})
export class UsersModule {
}
