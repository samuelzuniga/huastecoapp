import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '../material/material.module';

import {COMPONENT_DECLARATIONS, ENTRY_COMPONENTS} from './products.common';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		FlexLayoutModule,
		ReactiveFormsModule,
		MaterialModule
	],
	declarations: COMPONENT_DECLARATIONS,
	exports: COMPONENT_DECLARATIONS,
	entryComponents: ENTRY_COMPONENTS
})
export class ProductsModule {
}
