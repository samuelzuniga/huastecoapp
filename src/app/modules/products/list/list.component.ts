import {Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges} from '@angular/core';
import {MatTableDataSource} from '@angular/material';

import {IProduct} from '../../../../../server/src-shared/interfaces';

@Component({
	selector: 'app-huasteco-products-list',
	templateUrl: './list.component.html',
	styleUrls: ['./list.component.scss']
})
export class ProductsListComponent implements OnInit, OnChanges {
	@Input() products: IProduct[];
	@Input() hasAddCommand: boolean = false;
	@Output() addProduct: EventEmitter<IProduct> = new EventEmitter<IProduct>();
	displayedColumns: string[] = ['name', 'brand', 'unitaryPrice'];
	dataSource = null;

	constructor() {
	}

	ngOnInit() {
		const source = this.pushCommands();

		this.dataSource = new MatTableDataSource(source);
	}

	ngOnChanges(changes: SimpleChanges) {
		const productChanges = changes['products'];

		if (productChanges && productChanges.currentValue) {
			this.dataSource = new MatTableDataSource(productChanges.currentValue || []);
		}
	}

	pushCommands(): any[] {
		const source = this.products.slice();

		if (this.hasAddCommand) {
			const addCommand = 'addCommand';
			this.displayedColumns.unshift(addCommand);
			source.forEach((item: any) => {
				item[addCommand] = '';
			});
		}

		return source;
	}

	onAddProduct(rowItem: any): void {
		const {id, name, brand, unitaryPrice} = rowItem;

		this.addProduct.emit({id, name, brand, unitaryPrice});
	}

}
