import {Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {OrdersService} from '../../orders/services/orders.service';

import {IProduct, IProductOrder} from '../../../../../server/src-shared/interfaces';

@Component({
	selector: 'app-huasteco-products-order-list',
	templateUrl: './products-order-list.component.html',
	styleUrls: ['./products-order-list.component.scss']
})
export class ProductsOrderListComponent implements OnInit, OnChanges {
	@Input() order: IProductOrder[];
	@Input() hasEdit: boolean ;
	@Input() hasDelete: boolean ;
	@Output() editProduct: EventEmitter<IProductOrder> = new EventEmitter<IProductOrder>();
	@Output() removeProduct: EventEmitter<IProductOrder> = new EventEmitter<IProductOrder>();
	displayedColumns: string[] = ['name', 'brand', 'unitaryPrice', 'quantity', 'subtotal'];
	dataSource = null;

	constructor(private orderService: OrdersService) {
	}

	ngOnInit() {
		const source = (this.order || []).slice();
		if (this.hasDelete) {
			this.displayedColumns.unshift('deleteCommand');
		}
		if (this.hasEdit) {
			this.displayedColumns.unshift('editCommand');
		}
		this.dataSource = new MatTableDataSource(source);
	}

	/** On change input order property update the data source. */
	ngOnChanges(changes: SimpleChanges) {
		const orderChanges = changes['order'];

		if (orderChanges && orderChanges.currentValue) {
			this.dataSource = (orderChanges.currentValue || []).slice();
		}
	}

	/** Notify to the host the intention of edit the product. */
	onEditProduct(productOrder: IProductOrder): void {
		this.editProduct.emit(productOrder);
	}

	/** Notify to the host the intention of remove the product. */
	onRemoveProduct(productOrder: IProductOrder): void {
		this.removeProduct.emit(productOrder);
	}

	/** Gets the total cost of all transactions. */
	getTotalCost() {
		return this.order.map(po => po.subtotal).reduce((acc, value) => acc + value, 0);
	}

}
