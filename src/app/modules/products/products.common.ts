import { Routes } from '@angular/router';
import {ProductsListComponent} from './list/list.component';
import {AddEditProductComponent} from './add-edit-product/add-edit-product.component';
import {ProductsOrderListComponent} from './products-order-list/products-order-list.component';

export const COMPONENT_DECLARATIONS: any[] = [
	ProductsListComponent,
	AddEditProductComponent,
	ProductsOrderListComponent
];

export const PROVIDERS_DECLARATIONS: any[] = [
];

export const ROUTES: Routes = [
];

export const ENTRY_COMPONENTS: any[] = [
	AddEditProductComponent
];
