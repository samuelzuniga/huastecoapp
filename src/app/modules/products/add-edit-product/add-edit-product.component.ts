import { Component, Inject, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import {
	FormBuilder, FormControl,
	FormGroup,
	Validators
} from '@angular/forms';
import {IProduct, IProductOrder} from '../../../../../server/src-shared/interfaces';


@Component({
  selector: 'app-huasteco-product-confirm',
  templateUrl: './add-edit-product.component.html',
  styleUrls: ['./add-edit-product.component.scss']
})
export class AddEditProductComponent implements OnInit {
	confirmProductForm: FormGroup;
	productOrder: IProductOrder;

	constructor(
		private formBuilder: FormBuilder,
		@Inject(MAT_DIALOG_DATA) public data: any) {}

	ngOnInit() {
		this.productOrder = this.data;

		this.confirmProductForm = this.formBuilder.group({
			quantity: [this.data.quantity, [Validators.required, Validators.min(0.1)]],
			productId: [this.productOrder.product.id],
			name: new FormControl({value: this.productOrder.product.name, disabled: true}, Validators.required),
			brand: new FormControl({value: this.productOrder.product.brand, disabled: true}, Validators.required),
			unitaryPrice: new FormControl({value: this.productOrder.product.unitaryPrice, disabled: true}, Validators.required)
		});
	}

	onSubmit() {
	}

	getEnteredProduct(): IProductOrder {
		const quantity = parseFloat(this.confirmProductForm.value.quantity);

		const productOrder: IProductOrder = {
			product: this.productOrder.product,
			quantity,
			date: new Date(),
			subtotal: quantity * this.productOrder.product.unitaryPrice
		};

		return productOrder;
	}

}
