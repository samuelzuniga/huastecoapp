import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {MaterialModule} from '../material/material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule} from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import {COMPONENT_DECLARATIONS} from './auth.common';
import {AuthRoutingModule} from './auth-routing.module';


@NgModule({
	imports: [
		CommonModule,
		FlexLayoutModule,
		FormsModule,
		ReactiveFormsModule,
		AuthRoutingModule,
		MaterialModule,
		SharedModule
	],
	declarations: COMPONENT_DECLARATIONS,
	providers: [],

})
export class AuthModule {
}
