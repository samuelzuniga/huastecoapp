import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {
	FormBuilder,
	FormGroup,
	Validators
} from '@angular/forms';
import {UsersService} from '../../../root-services/api/users.service';
import {IPasswordChange} from '../../../../../server/src-shared/interfaces';
import {PasswordValidation} from '../../shared/helpers/password-match-validator.helper';
import {IClientError} from '../../shared/interfaces/client-error.interface';
import {FlashMessageService} from '../../core/services/flash-message.service';

@Component({
	selector: 'app-huasteco-set-own-password',
	templateUrl: './change-password.component.html',
	styleUrls: ['./change-password.component.scss']
})
export class AuthSetOwnPasswordComponent implements OnInit {
	username: string;
	isLinear: boolean;
	temporaryPasswordForm: FormGroup;
	confirmPasswordForm: FormGroup;
	minPasswordLength = 8;
	isSubmitting: boolean;

	constructor(private route: ActivatedRoute,
				private flashMessageService: FlashMessageService,
				private router: Router,
				private formBuilder: FormBuilder,
				private adminAccountService: UsersService) {
	}

	ngOnInit() {
		this.isSubmitting = false;
		const passwordValidators = [Validators.required, Validators.minLength(this.minPasswordLength)];
		this.isLinear = true;
		this.route.queryParams
			.subscribe(params => {
				this.username = params.username;
			});

		this.temporaryPasswordForm = this.formBuilder.group({
			username: [this.username, [Validators.required]],
			password: ['', passwordValidators]
		});

		this.confirmPasswordForm = this.formBuilder.group({
			password: ['', passwordValidators],
			passwordConfirm: ['', passwordValidators]
		}, {validator: PasswordValidation.MatchPassword});
	}

	onChangePassword(): void {
		const {username, password} = this.temporaryPasswordForm.value;
		const credentials: IPasswordChange = {
			username,
			password,
			newPassword: this.confirmPasswordForm.value.password
		};

		this.isSubmitting = true;
		this.adminAccountService.forceChangePassword(credentials)
			.subscribe((userTokens) => {
				this.isSubmitting = false;
				this.flashMessageService.showSuccess('Contraseña cambiada con éxito');
				this.router.navigate(['/auth', 'login']);
			}, (error: IClientError) => {
				this.isSubmitting = false;
				this.flashMessageService.showError(error.message);
			});
	}

	onSubmitTemporaryPasswordForm(): void {
	}

	onSubmitSecondForm(): void {
	}

	controlHasError(form: FormGroup, controlName: string, errorType: string): boolean {
		return form.controls &&
			form.controls[controlName] &&
			form.controls[controlName].touched &&
			form.controls[controlName].errors &&
			form.controls[controlName].errors[errorType];
	}

}
