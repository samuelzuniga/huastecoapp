import { Routes } from '@angular/router';
import {AuthLoginComponent } from './login/login.component';
import {AuthSetOwnPasswordComponent} from './change-password/change-password.component';
import {AuthHomeComponent} from './auth-home/auth-home.component';

export const COMPONENT_DECLARATIONS: any[] = [
	AuthLoginComponent,
	AuthHomeComponent,
	AuthSetOwnPasswordComponent
];

export const PROVIDERS_DECLARATIONS: any[] = [
];

export const ROUTES: Routes = [
	{
		path: '',
		component: AuthHomeComponent,
		children: [
			{
				path: 'set-own-password',
				component: AuthSetOwnPasswordComponent
			},
			{
				path: 'login',
				component: AuthLoginComponent
			}
		]
	}
];
