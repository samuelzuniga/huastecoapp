import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {
	FormBuilder,
	FormGroup,
	Validators
} from '@angular/forms';

import {validateFormControls} from '../../shared/helpers/input-validation';
import {IClientError} from '../../shared/interfaces/client-error.interface';
import {FlashMessageService} from '../../core/services/flash-message.service';
import {AuthService} from '../../../root-services/api/auth.service';
import {IUserAuthenticated} from '../../../../../server/src-shared/interfaces';

@Component({
	selector: 'app-huasteco-auth-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class AuthLoginComponent implements OnInit {
	loginForm: FormGroup;
	controlHasError: Function;
	isSubmitting: boolean;

	constructor(
		private router: Router,
		private flashMessageService: FlashMessageService,
		private formBuilder: FormBuilder,
		private authService: AuthService) {
	}

	ngOnInit() {
		this.isSubmitting = false;
		this.loginForm = this.formBuilder.group({
			username: ['', [Validators.required]],
			password: ['', [Validators.required]]
		});

		this.controlHasError = validateFormControls(this.loginForm);
	}

	onSubmit() {
		this.isSubmitting = true;
		this.authService.authenticateUser(this.loginForm.value)
			.then((user: IUserAuthenticated) => {
				this.isSubmitting = false;
				this.flashMessageService.showSuccess(`Bienvenido ${user.username}`);
				this.router.navigate(['/']);
			})
			.catch((error: IClientError) => {
				this.isSubmitting = false;
				this.flashMessageService.showError(error.message);
			});
	}
}
