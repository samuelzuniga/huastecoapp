import { Routes } from '@angular/router';

// components
import {DealersHomeComponent} from './dealers-home/dealers-home.component';
import {DealersCreateComponent} from './create/create.component';

// services
import {GetAuthenticatedUserResolver} from '../../root-services/resolvers/get-authenticated-user.resolver';
import {DealersService} from './services/dealers.service';

export const COMPONENT_DECLARATIONS: any[] = [
	DealersHomeComponent,
	DealersCreateComponent
];

export const PROVIDERS_DECLARATIONS: any[] = [
	DealersService
];

export const ROUTES: Routes = [
	{
		path: '',
		component: DealersHomeComponent,
		children: [
			{
				path: 'create',
				component: DealersCreateComponent,
				resolve: {
					user: GetAuthenticatedUserResolver
				}
			}
		]
	}
];
