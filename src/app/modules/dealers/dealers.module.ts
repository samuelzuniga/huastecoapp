import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';

// modules
import {MaterialModule} from '../material/material.module';
import {DealersRoutingModule} from './dealers-routing.module';
import {SharedModule} from '../shared/shared.module';
import {PersonModule} from '../person/person.module';

import {COMPONENT_DECLARATIONS, PROVIDERS_DECLARATIONS} from './dealers.common';

@NgModule({
	imports: [
		CommonModule,
		FlexLayoutModule,
		FormsModule,
		ReactiveFormsModule,
		DealersRoutingModule,
		MaterialModule,
		SharedModule,
		PersonModule
	],
	providers: [
		...PROVIDERS_DECLARATIONS
	],
	declarations: [
		...COMPONENT_DECLARATIONS
	],
})
export class DealersModule {
}
