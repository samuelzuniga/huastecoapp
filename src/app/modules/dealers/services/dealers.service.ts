import {Injectable} from '@angular/core';
import {
	catchError,
	map
} from 'rxjs/operators';
import {
	Observable,
} from 'rxjs';
import {
	HttpClient,
} from '@angular/common/http';

import {environment} from '../../../../environments/environment';
import {IClientError} from '../../shared/interfaces/client-error.interface';
import {IResourceDealer} from '../../../../../server/src-shared/interfaces';
import {IDealer} from '../../../../../server/src-shared/interfaces';


import {mapToClientError} from '../../shared/helpers/http-error-handler';
import {getHttpHeadersWithApiKeys} from '../../shared/helpers/http-helper';


const httpOptions = {
	headers: getHttpHeadersWithApiKeys()
};

@Injectable()
export class DealersService {
	private entity = 'dealers';

	constructor( private http: HttpClient) {
	}

	createDealer(user: IDealer): Observable<IDealer | IClientError> {
		const body = JSON.stringify(user);
		const url = `${environment.servicesURL}/${this.entity}`;

		return this.http.post<IResourceDealer | IClientError>(url, body, httpOptions)
			.pipe(
				map((resource: IResourceDealer) => resource.data.attributes),
				catchError(mapToClientError)
			);
	}
}
