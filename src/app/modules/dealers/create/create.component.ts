import {
	Component,
	OnInit
} from '@angular/core';
import {
	FormBuilder,
	FormGroup,
	Validators
} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {validateFormControls} from '../../shared/helpers/input-validation';
import {IDealer, IUserAuthenticated} from '../../../../../server/src-shared/interfaces';
import {IClientError} from '../../shared/interfaces/client-error.interface';
import {INTERNATIONAL_PHONE_NUMBER_PATTERN} from '../../shared/helpers/patterns';
import {FlashMessageService} from '../../core/services/flash-message.service';
import {environment} from '../../../../environments/environment';
import {DealersService} from '../services/dealers.service';

@Component({
	selector: 'app-huasteco-dealers-create',
	templateUrl: './create.component.html',
	styleUrls: ['./create.component.scss']
})
export class DealersCreateComponent implements OnInit {
	createUserForm: FormGroup;
	controlHasError: Function;
	isSubmitting: boolean;
	user: IUserAuthenticated;
	defaultPictureImage: string;
	test1: string;

	constructor(
		private flashMessageService: FlashMessageService,
		private route: ActivatedRoute,
		private router: Router,
		private formBuilder: FormBuilder,
		private dealersService: DealersService) {
	}

	ngOnInit() {
		this.isSubmitting = false;
		this.user = this.route.snapshot.data.user;
		this.defaultPictureImage = environment.defaultUserImage;

		this.createUserForm = this.formBuilder.group({
			companyName: [this.user.company.name],
			pictureURL: [''],
			companyId: [this.user.company.id, [Validators.required]],
			username: ['', [Validators.required]],
			firstName: ['', [Validators.required]],
			lastName: ['', [Validators.required]],
			phoneNumber: ['', [Validators.required, Validators.pattern(INTERNATIONAL_PHONE_NUMBER_PATTERN)]],
			email: ['', [Validators.required, Validators.email]]
		});

		this.controlHasError = validateFormControls(this.createUserForm);
	}

	onSubmit() {
		this.isSubmitting = true;
		this.dealersService.createDealer(this.createUserForm.value)
			.subscribe((user: IDealer) => {
				this.isSubmitting = false;
				this.flashMessageService.showSuccess(`Repartidor ${user.username} creado exitosamente`);
				this.router.navigate(['/']);
			}, (error: IClientError) => {
				this.isSubmitting = false;
				this.flashMessageService.showError(error.message);
			});
	}

	onSelectImage(imageName: string) {
		this.createUserForm.controls['pictureURL'].setValue(imageName);
	}
}
