import { Routes } from '@angular/router';
import {MapComponent} from './map/map.component';

export const COMPONENT_DECLARATIONS: any[] = [
	MapComponent
];

export const PROVIDERS_DECLARATIONS: any[] = [
];

export const ROUTES: Routes = [
];
