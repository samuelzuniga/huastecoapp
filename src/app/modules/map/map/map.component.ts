import {
	Component,
	OnInit,
	Input
} from '@angular/core';
import {MapService} from '../services/map.service';

@Component({
	selector: 'app-huasteco-map',
	templateUrl: './map.component.html',
	styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
	@Input() latitude: number;
	@Input() longitude: number;
	@Input() zoom: number;
	defaultZoom: number ;
	constructor(private mapService: MapService) {
	}

	ngOnInit() {
		this.setDefaults();
	}

	setDefaults(): void {
		this.defaultZoom = 17;
	}
}
