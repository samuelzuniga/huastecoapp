import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AgmCoreModule} from '@agm/core';
import {COMPONENT_DECLARATIONS} from './map.common';
import {MapService} from './services/map.service';
import {GeoCodingService} from './services/geocoding.service';

@NgModule({
	providers: [
		GeoCodingService,
		MapService
	],
	imports: [
		CommonModule,
		AgmCoreModule
	],
	declarations: [
		...COMPONENT_DECLARATIONS
	],
	exports : [
		...COMPONENT_DECLARATIONS
	]
})
export class MapModule {
}
