import {Injectable} from '@angular/core';
import {ICoordinates} from '../../../../../server/src-shared/interfaces/coordinates';
import {GeoCodingService} from './geocoding.service';

@Injectable()
export class MapService {
	lastKnownCoordinates: ICoordinates;
	constructor() {
		this.lastKnownCoordinates = null;
	}

	getCurrentLocation(): Promise<ICoordinates> {
		return new Promise((resolve, reject) => {
			if (this.getNavigator().geolocation) {
				return this.getNavigator().geolocation.getCurrentPosition((position: {coords: ICoordinates}) => {
					this.lastKnownCoordinates = {
						latitude: position.coords.latitude,
						longitude: position.coords.longitude
					};
					return resolve(this.lastKnownCoordinates);
				});
			}
			return this.lastKnownCoordinates ? resolve(this.lastKnownCoordinates) : reject(null);
		});
	}

	private getNavigator(): any {
		return navigator;
	}

}
