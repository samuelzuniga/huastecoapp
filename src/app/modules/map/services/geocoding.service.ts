import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {mapToClientError} from '../../shared/helpers/http-error-handler';
import {catchError, map} from 'rxjs/operators';
import {IGeoCoderRequest} from '../../../../../server/src-shared/interfaces';
import {IClientError} from '../../shared/interfaces/client-error.interface';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {HttpHeaders} from '@angular/common/http';


// todo move it to shared http helpers
const httpOptions = {
	headers: new HttpHeaders({
		'Content-Type': 'application/json',
		'x-api-key': environment.huastecoAPIKey
	})
};

@Injectable()
export class GeoCodingService {
	constructor(private http: HttpClient) {
	}

	geocode(request: IGeoCoderRequest): Observable<any | IClientError> {
		const body = JSON.stringify(request);
		const url = `${environment.servicesURL}/maps/geocode`;

		return this.http.post<any | IClientError>(url, body, httpOptions)
			.pipe(
				map((resource: any) => {
					return (
							resource &&
							resource.json &&
							resource.json.results &&
							resource.json.results.shift()
							) || null;
				}),
				catchError(mapToClientError)
			);
	}
}
