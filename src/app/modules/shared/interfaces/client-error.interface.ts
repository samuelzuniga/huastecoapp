export interface IClientError {
	code: string;
	name: string;
	message: string;
	thrownByServer: boolean;
}
