export interface IMenuItem {
	label: string;
	paths: string[];
	groups?: string[];
}