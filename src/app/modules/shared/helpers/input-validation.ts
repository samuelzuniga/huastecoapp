import {FormGroup} from '@angular/forms';

export function validateFormControls(form: FormGroup): Function {
	return (controlName: string, errorType: string) => {
		return form.controls &&
			form.controls[controlName] &&
			form.controls[controlName].touched &&
			form.controls[controlName].errors &&
			form.controls[controlName].errors[errorType];
	};
}

