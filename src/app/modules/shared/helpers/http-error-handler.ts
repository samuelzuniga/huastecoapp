import {Observable, throwError} from 'rxjs';
import {HttpErrorResponse} from '@angular/common/http';
import {IClientError} from '../interfaces/client-error.interface';


export function mapToClientError(errorResponse: HttpErrorResponse): Observable<IClientError> {
	const error = errorResponse.error ? errorResponse.error : errorResponse;

	const unknown = 'unknown';
	if (error instanceof ErrorEvent) {
		return throwError({
			message: error.message,
			code: unknown,
			name: unknown,
			thrownByServer: false
		});
	}
	const {name = unknown, message = unknown, code = unknown} = error;

	return throwError({name, message, code, thrownByServer: Boolean(errorResponse.error)});
}