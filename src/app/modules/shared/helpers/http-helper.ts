import {HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../environments/environment';

export function getHttpHeadersWithApiKeys() {
	 return new HttpHeaders(
	 	{
			'Content-Type': 'application/json',
			'x-api-key': environment.huastecoAPIKey
	 	});
}

export function addIdToAttributes(data: any): any {
	return { id: data.id,  ...data.attributes };
}
