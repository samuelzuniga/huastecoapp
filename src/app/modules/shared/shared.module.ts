import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '../material/material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {
	FormsModule,
	ReactiveFormsModule
} from '@angular/forms';

import {
	COMPONENT_DECLARATIONS,
	PROVIDERS_DECLARATIONS,
	PIPES_DECLARATIONS
} from './shared.common';
import {MyTelInput} from './telephone/telephone.component';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		FlexLayoutModule,
		ReactiveFormsModule,
		MaterialModule
	],
	declarations: [
		...COMPONENT_DECLARATIONS,
		...PIPES_DECLARATIONS,
		MyTelInput
	],
	entryComponents: [
		...COMPONENT_DECLARATIONS
	],
	providers: [
		...PROVIDERS_DECLARATIONS
	],
	exports : [
		CommonModule,
		...COMPONENT_DECLARATIONS,
		...PIPES_DECLARATIONS
	]
})
export class SharedModule {
}
