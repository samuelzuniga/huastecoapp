import {FocusMonitor} from '@angular/cdk/a11y';
import {coerceBooleanProperty} from '@angular/cdk/coercion';
import {Component, ElementRef, Input, OnDestroy} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatFormFieldControl} from '@angular/material';
import {OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {validateFormControls} from '../helpers/input-validation';

/** Data structure for holding telephone number. */
export class MyTel {
	constructor(public internationalCode: string, public areaCode: string, public subscriber: string) {
	}
}


/** Custom `MatFormFieldControl` for telephone number input. */
@Component({
	selector: 'app-huasteco-telephone-input',
	templateUrl: 'telephone.component.html',
	styleUrls: ['telephone.component.scss'],
	providers: [{provide: MatFormFieldControl, useExisting: MyTelInput}],
	host: {
		'[class.floating]': 'shouldLabelFloat',
		'[id]': 'id',
		'[attr.aria-describedby]': 'describedBy',
	}
})
export class MyTelInput implements MatFormFieldControl<MyTel>, OnDestroy {
	static nextId = 0;

	parts: FormGroup;
	stateChanges = new Subject<void>();
	focused = false;
	ngControl = null;
	errorState = false;
	controlType = 'app-huasteco-telephone-input';
	id = `app-huasteco-telephone-input-${MyTelInput.nextId++}`;
	describedBy = '';

	constructor(fb: FormBuilder, private fm: FocusMonitor, private elRef: ElementRef<HTMLElement>) {
		this.parts = fb.group({
			internationalCode: '',
			areaCode: '',
			subscriber: '',
		});

		fm.monitor(elRef.nativeElement, true).subscribe(origin => {
			this.focused = !!origin;
			this.stateChanges.next();
		});
	}

	get empty() {
		const {value: {internationalCode, areaCode, subscriber}} = this.parts;

		return !internationalCode && !areaCode && !subscriber;
	}

	get shouldLabelFloat() {
		return this.focused || !this.empty;
	}

	private _placeholder: string;

	@Input()
	get placeholder(): string {
		return this._placeholder;
	}

	set placeholder(value: string) {
		this._placeholder = value;
		this.stateChanges.next();
	}

	private _required = false;

	@Input()
	get required(): boolean {
		return this._required;
	}

	set required(value: boolean) {
		this._required = coerceBooleanProperty(value);
		this.stateChanges.next();
	}

	private _disabled = false;

	@Input()
	get disabled(): boolean {
		return this._disabled;
	}

	set disabled(value: boolean) {
		this._disabled = coerceBooleanProperty(value);
		this.stateChanges.next();
	}

	@Input()
	get value(): MyTel | null {
		const {value: {internationalCode, areaCode, subscriber}} = this.parts;
		if (internationalCode.length === 2 && areaCode.length === 3 && subscriber.length === 7) {
			return new MyTel(internationalCode, areaCode, subscriber);
		}
		return null;
	}

	set value(tel: MyTel | null) {
		const {internationalCode, areaCode, subscriber} = tel || new MyTel('', '', '');
		this.parts.setValue({internationalCode, areaCode, subscriber});
		this.stateChanges.next();
	}

	ngOnDestroy() {
		this.stateChanges.complete();
		this.fm.stopMonitoring(this.elRef.nativeElement);
	}

	setDescribedByIds(ids: string[]) {
		this.describedBy = ids.join(' ');
	}

	onContainerClick(event: MouseEvent) {
		if ((event.target as Element).tagName.toLowerCase() != 'input') {
			this.elRef.nativeElement.querySelector('input')!.focus();
		}
	}
}


/** @title Form field with custom telephone number input control. */
@Component({
	selector: 'app-huasteco-telephone',
	styles: [`
        mat-form-field {
            width: 100%;
        }
	`
	],
	template: `
		<form action="" [formGroup]="telephoneForm">
			<mat-form-field class="form__field">
				<app-huasteco-telephone-input placeholder="Número celular" [value]="value" formControlName="telephone"
											  required></app-huasteco-telephone-input>
				<mat-icon matSuffix>phone</mat-icon>
				<mat-hint align="start" *ngIf="controlHasError('telephone', 'required' )">El campo es obligatorio</mat-hint>
				<!--<mat-hint>Include area code</mat-hint>-->
			</mat-form-field>
		</form>

	`
})
export class TelephoneComponent implements OnInit {
	telephoneForm: FormGroup;
	controlHasError: Function;
	value: MyTel;

	constructor(private formBuilder: FormBuilder) {
		this.value = new MyTel('52', '782', '1282069');
	}

	ngOnInit() {
		this.telephoneForm = this.formBuilder.group({
			telephone: [this.value, [Validators.required]]
		});
		this.controlHasError = validateFormControls(this.telephoneForm);
	}

}


/**  Copyright 2018 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that
 can be found in the LICENSE file at http://angular.io/license */