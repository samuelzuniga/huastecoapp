import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {finalize} from 'rxjs/operators';
import {UploadFileService} from '../services/upload-file.service';


@Component({
	selector: 'app-huasteco-upload-picture',
	templateUrl: './upload-picture.component.html',
	styleUrls: ['./upload-picture.component.scss']
})
export class UploadPictureComponent implements OnInit {
	@Input() defaultImage: string;
	@Output() select: EventEmitter<string> = new EventEmitter<string>();

	imageURL: string;
	isLoading: boolean;
	constructor(private uploadFileService: UploadFileService) {
	}

	ngOnInit() {
		this.imageURL = this.getImageURL(this.defaultImage);
		this.isLoading = false;
	}

	private getImageURL(fileName: string): string {
		return `${environment.uploadBucket}/${fileName}`;
	}

	onFileSelected(files: any[]) {
		const file = Array.from(files).pop();

		if (!file) {return;}

		this.isLoading = true;
		const reader = new FileReader();
		reader.addEventListener('loadend', (e) => {
			const payload = {name: file.name, type: file.type};
			this.uploadFileService.uploadFile(payload, reader.result)
				.pipe(
					finalize(() => this.isLoading = false)
				)
				.subscribe(() => {
						this.imageURL = this.getImageURL(file.name);
						this.select.emit(file.name);
					},
					(err) => {
						console.log('There isn an error');
						console.log(err);
					});
		});

		reader.readAsArrayBuffer(file);
	}
}
