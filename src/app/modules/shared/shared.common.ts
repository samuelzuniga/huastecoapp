import { Routes } from '@angular/router';

import {CardComponent} from './card/card.component';
import {TelephoneComponent} from './telephone/telephone.component';
import {UploadPictureComponent} from './upload-picture/upload-picture.component';
import {UploadFileService} from './services/upload-file.service';
import {SafeHtmlPipe} from './pipes/safeHtml';

export const COMPONENT_DECLARATIONS: any[] = [
	CardComponent,
	TelephoneComponent,
	UploadPictureComponent
];

export const PIPES_DECLARATIONS: any[] = [
	SafeHtmlPipe
];

export const PROVIDERS_DECLARATIONS: any[] = [
	UploadFileService
];

export const ROUTES: Routes = [];
