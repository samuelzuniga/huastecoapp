import {Injectable} from '@angular/core';
import {
	catchError,
	switchMap,
	map
} from 'rxjs/operators';
import {
	Observable,
} from 'rxjs';
import {
	HttpClient, HttpHeaders
} from '@angular/common/http';

import {IClientError} from '../interfaces/client-error.interface';

import {getHttpHeadersWithApiKeys} from '../helpers/http-helper';

import {mapToClientError} from '../helpers/http-error-handler';
import {environment} from '../../../../environments/environment';

const httpOptions = {
	headers: getHttpHeadersWithApiKeys()
};


@Injectable()
export class UploadFileService {
	constructor(private http: HttpClient) {
	}

	uploadFile(payload: {name: string, type: string}, fileContent: any): Observable<any | IClientError> {
		return this.getURLToUpload(payload)
			.pipe(
				switchMap((url: string) => this.uploadToS3URL(url, fileContent, payload.type))
			);
	}

	private getURLToUpload(payload: {name: string, type: string}): Observable<string | IClientError> {
		const url = `${environment.servicesURL}/file`;

		return this.http.post<string | IClientError>(url, JSON.stringify(payload),  httpOptions)
			.pipe(
				map((response: any) => response.uploadURL),
				catchError(mapToClientError)
			);
	}

	private uploadToS3URL(url: string, fileContent: any, fileType: string): Observable<any | IClientError> {
		const options =  {
			headers: new HttpHeaders(
				{
					[environment.headers.huastecoIgnoreHttpInterceptors]: 'true'
				})
		};

		const body = new Blob([fileContent], {type: fileType});

		return this.http.put<any | IClientError>(url, body, options)
			.pipe(
				map((response: any) => response),
				catchError(mapToClientError)
			);
	}


}
