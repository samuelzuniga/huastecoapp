import {Routes} from '@angular/router';
import {SearchPersonInputComponent} from './search-person-input/search-person-input.component';
import {PersonListComponent} from './person-list/person-list.component';
import {FindPersonComponent} from './find-person/find-person.component';
import {AddressListComponent} from './address-list/address-list.component';

export const COMPONENTS_DECLARATIONS = [
	FindPersonComponent,
	SearchPersonInputComponent,
	PersonListComponent,
	AddressListComponent
];

export const COMPONENTS_EXPORTED = [
	FindPersonComponent
];

export const PROVIDERS_DECLARATIONS: any[] = [];

export const ROUTES: Routes = [];
