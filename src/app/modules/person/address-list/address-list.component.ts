import {Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges} from '@angular/core';
import {IAddress} from '../../../../../server/src-shared/interfaces';

@Component({
  selector: 'app-huasteco-address-list',
  templateUrl: './address-list.component.html',
  styleUrls: ['./address-list.component.scss']
})
export class AddressListComponent implements OnInit, OnChanges {
  @Input() addresses: IAddress [];
  @Output() selectAddress: EventEmitter<IAddress> = new EventEmitter<IAddress>();

  selectedAddressId: string;
  constructor() { }

  ngOnInit() {
    this.selectedAddressId = '';
  }

  ngOnChanges(changes: SimpleChanges) {
    this.selectedAddressId = '';
  }

  onSelectAddress(address: IAddress): void {
    this.selectedAddressId = address.id;
    this.selectAddress.emit(address);
  }

}
