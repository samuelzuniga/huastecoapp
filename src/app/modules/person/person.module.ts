import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MaterialModule} from '../material/material.module';
import {SharedModule} from '../shared/shared.module';
import {COMPONENTS_DECLARATIONS, COMPONENTS_EXPORTED,  PROVIDERS_DECLARATIONS} from './person.common';

@NgModule({
	imports: [
		CommonModule,
		SharedModule,
		FormsModule,
		FlexLayoutModule,
		MaterialModule
	],
	declarations: COMPONENTS_DECLARATIONS,
	providers: PROVIDERS_DECLARATIONS,
	exports: COMPONENTS_EXPORTED
})
export class PersonModule {
}
