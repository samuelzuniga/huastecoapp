/*
	Allow to select a customer and his corresponding address
	Display the controls to search customers|dealers|operators by phone|name
	Currently the service to search all/single entities is passed as an input property
	As far the user captures the search text, the control executes a query to get the person matching results
	Getting the person results display them as a list
	If displayAddresses is set on, display the list of addresses
	Emit values, just until some customer and address(if it is required) are selected
 */

import {
	Component,
	EventEmitter,
	OnInit,
	Input,
	Output,
	ViewChild
} from '@angular/core';

import {Subject, Observable} from 'rxjs';

import {
	filter,
	flatMap,
	map,
	scan,
	tap
} from 'rxjs/operators';

import {
	IAddress,
	IPerson,
} from '../../../../../server/src-shared/interfaces';

import {ActivatedRoute} from '@angular/router';
import {IFindInput} from '../interfaces/find-input.';
import {SearchPersonInputComponent} from '../search-person-input/search-person-input.component';
import {PersonListComponent} from '../person-list/person-list.component';
import {IFindPersonResult} from '../interfaces/find-person-result';

export interface FindPersonConfiguration {
	findAllMethod: Function,
	findOneMethod: Function
}

interface IFindPersonState {
	addresses: IAddress[];
	persons: IPerson[];
	search: string;
	selectedAddress: IAddress;
	selectedPerson: IPerson;
}

@Component({
	selector: 'app-huasteco-find-person',
	templateUrl: './find-person.component.html',
	styleUrls: ['./find-person.component.scss']
})
export class FindPersonComponent implements OnInit {
	@Input() findConfiguration: FindPersonConfiguration;
	@Input() displayAddresses = true;
	@Output() selectPerson: EventEmitter<IFindPersonResult> = new EventEmitter<IFindPersonResult>();
	@Output() clearPerson: EventEmitter<void> = new EventEmitter<void>();

	@ViewChild('searchPersonInput') searchPersonInput: SearchPersonInputComponent;
	@ViewChild('personList') personList: PersonListComponent;
	search: string;
	changeState: Subject<any>;
	state: IFindPersonState;

	constructor(private route: ActivatedRoute) {
	}

	ngOnInit() {
		this.state = this.getInitialState();
		this.changeState = new Subject<any>();

		// setup observables
		this.setupSearchPersonInputObservable();
		this.setupPersonListObservable();
		this.setupStateObservable();
	}

	/*
		Observe changes in the state and acumulatte the new values into the general component state
	 */
	private setupStateObservable() {
		this.changeState
			.pipe(scan(this.accumulateValueInState.bind(this), this.state))
			.subscribe((state: IFindPersonState) => {
				this.setState(state);
				this.notifyOnPersonAndAddressSelected(state);
			}, this.onError('setupStateObservable'));
	}

	/*
		On person and address selected emit the result to the client
	 */
	private notifyOnPersonAndAddressSelected(state: IFindPersonState) {
		if (this.isPersonAndAddressSet(state)) {
			this.selectPerson.emit(	{person: state.selectedPerson, address: state.selectedAddress});
		}
	}

	/*
		Accumulate the current value in the component state
	 */
	private accumulateValueInState(accumulator: any, current: any) {
		return Object.assign({}, accumulator, current);
	}

	/*
		Set the current state
	 */
	private setState(state: IFindPersonState) {
		this.state = state;
	}

	/*
		Determine if the person and address are both selected
	 */
	private isPersonAndAddressSet(state: IFindPersonState): boolean {
		return Boolean(state.selectedPerson && state.selectedAddress);
	}

	/*
		Query the service to get the person details
	 */
	private getPersonDetails(person: IPerson) {
		return this.findConfiguration.findOneMethod(person.id)
			.pipe(map((personInfo: IPerson) => ({selectedPerson: person, addresses: personInfo['addresses']})));
	}

	/*
		On person selected get the person details and update the state
	 */
	private setupPersonListObservable() {
		this.personList.select
			.pipe(flatMap(this.getPersonDetails.bind(this)))
			.subscribe((result) => this.changeState.next(result),
				this.onError('setupListObservable'));
	}

	/*
		Query the endpoint to get the persons whose match the query
	 */

	private getMatchingPersons(params: {findBy: IFindInput, search: string}): Observable<{persons: IPerson[], search: string}> {
		return this.findConfiguration.findAllMethod(params.findBy, params.search)
			.pipe(
				map(results => ({persons: results, search: params.search}))
			);
	}

	private setupSearchPersonInputObservable() {
		// Cleaning search input component
		this.searchPersonInput.clear
			.subscribe(() => {
				this.changeState.next({search: '', persons: [], addresses: []});
				this.clearPerson.emit();
			});

		// On changing search input component, get new data
		this.searchPersonInput.change
			.pipe(
				tap(this.clearPreviousResults.bind(this)),
				filter((find: IFindInput) => !!find.findBy && !!find.search),
				map((find: IFindInput) => ({findBy: find.findBy, search: find.search})),
				flatMap(this.getMatchingPersons.bind(this))
			)
			.subscribe((results: {persons: IPerson[], search: string}) => {
				this.changeState.next({...this.getInitialState(), ...results});
				this.clearPerson.emit();
			}, this.onError('setupSearchPersonInputObservable'));
	}

	/*
		Get the component initial state
	 */
	private getInitialState(): IFindPersonState {
		return  {
			addresses: [],
			persons: [],
			search: '',
			selectedAddress: null,
			selectedPerson: null
		};
	}

	/*
		Clear previous content of persons and addresses arrays
	 */
	private clearPreviousResults(): void {
		this.changeState.next({selectedAddress: null, selectedPerson: null, persons: [], addresses: []})
	}

	/*
		On select address update the state
	 */
	onSelectAddress(address: IAddress) {
		this.changeState.next({selectedAddress: address});
	}

	/*
		Send a error message to the console along with the causing function
	 */
	private onError(functionName: string) {
		return (error: any) => {
			console.error(`Error function:${functionName}`);
			console.error(error.message || error);
		};
	}


}
