import {IAddress, IPerson} from '../../../../../server/src-shared/interfaces';

export interface IFindPersonResult {
	person: IPerson;
	address: IAddress;
}