import {FindPersonType} from '../../../../../server/src-shared/enums/person';

export interface IFindInput {
	findBy: FindPersonType;
	search: string;
}