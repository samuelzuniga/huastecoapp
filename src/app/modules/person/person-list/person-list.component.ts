import {Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges} from '@angular/core';
import {ICustomer, IPerson} from '../../../../../server/src-shared/interfaces';

@Component({
	selector: 'app-huasteco-person-list',
	templateUrl: './person-list.component.html',
	styleUrls: ['./person-list.component.scss']
})
export class PersonListComponent implements OnInit, OnChanges {
	@Input() persons: IPerson[] | ICustomer[];
	@Input() highlightedText: string;
	@Output() select: EventEmitter<IPerson | ICustomer> = new EventEmitter<IPerson | ICustomer>();
	selectedPersonId: string;
	constructor() {
	}

	ngOnInit() {
	}

	ngOnChanges(changes: SimpleChanges) {
		this.selectedPersonId = null;
	}

	highlight(content: string) {
		if(!this.highlightedText) {
			return content;
		}
		return content.replace(new RegExp(this.highlightedText, 'gi'), match => {
			return '<span style="background-color: yellow">' + match + '</span>';
		});
	}

	onSelectPerson(person: IPerson | ICustomer) {
		this.selectedPersonId = person.id;
		this.persons = [person];
		this.select.emit(person);
	}
}
