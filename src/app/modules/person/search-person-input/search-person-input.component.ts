import {
	Component,
	EventEmitter,
	OnDestroy,
	OnInit,
	Output
} from '@angular/core';

import {
	BehaviorSubject,
	combineLatest,
	Subject
} from 'rxjs';

import {
	debounceTime,
	distinctUntilChanged,
	filter,
	map,
	takeUntil,
	tap,
	skip
} from 'rxjs/operators';

import {FindPersonType} from '../../../../../server/src-shared/enums/person';
import {IFindInput} from '../interfaces/find-input.';

@Component({
	selector: 'app-huasteco-search-person-input',
	templateUrl: './search-person-input.component.html',
	styleUrls: ['./search-person-input.component.scss']
})
export class SearchPersonInputComponent implements OnInit, OnDestroy {
	@Output() change: EventEmitter<IFindInput> = new EventEmitter<IFindInput>();
	@Output() clear: EventEmitter<void> = new EventEmitter<void>();
	private destroy: Subject<void>;
	private searchText: BehaviorSubject<string>;
	private searchType: BehaviorSubject<FindPersonType>;
	FindPersonEnum = FindPersonType ;

	ngOnInit() {
		this.destroy = new Subject<void>();
		this.searchText = new BehaviorSubject<string>('');
		this.searchType = new BehaviorSubject<FindPersonType>(FindPersonType.phoneNumber);

		combineLatest(this.searchText, this.searchType)
			.pipe(
				takeUntil(this.destroy),
				debounceTime(1000),
				distinctUntilChanged(),
				map(([search, findBy]) => ({search, findBy})),
				filter((results) => !this.isStringEmpty(results.search)),
				tap(() => console.log('Emitting changed value'))
			)
			.subscribe((results) => {
				const {search, findBy} = results;
				this.change.emit({findBy, search});
			});

		// Whenever the search text box is set to empty string
		// Notify to the client about a clear event
		this.searchText
			.pipe(
				takeUntil(this.destroy),
				skip(1),  // skip initial value
				filter((search: string) => this.isStringEmpty(search)),
				tap(() => console.log('Emitting clear value'))
			)
			.subscribe(() => this.clear.emit());
	}

	ngOnDestroy() {
		this.destroy.next();
	}

	onChangePersonType(type: FindPersonType): void {
		this.searchType.next(type);
	}

	onChangeSearch(search: string): any {
		this.searchText.next(search);
	}

	private isStringEmpty(value: string): boolean {
		return !value || value.trim() === '';
	}
}
