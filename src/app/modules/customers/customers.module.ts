import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FlexLayoutModule} from '@angular/flex-layout';
import {
	FormsModule,
	ReactiveFormsModule
} from '@angular/forms';

import {CustomersRoutingModule} from './customers-routing.module';
import {COMPONENT_DECLARATIONS, PROVIDERS_DECLARATIONS} from './customers.common';
import {MaterialModule} from '../material/material.module';
import {SharedModule} from '../shared/shared.module';
import {MapModule} from '../map/map.module';
import {PersonModule} from '../person/person.module';


@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		FlexLayoutModule,
		MaterialModule,
		CustomersRoutingModule,
		PersonModule,
		MapModule,
		SharedModule
	],
	providers: [
		...PROVIDERS_DECLARATIONS
	],
	exports: [
		...COMPONENT_DECLARATIONS
	],
	declarations: [
		...COMPONENT_DECLARATIONS
	]
})
export class CustomersModule {
}
