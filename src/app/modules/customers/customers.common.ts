import { Routes } from '@angular/router';
import {CustomersCreateComponent} from './create/create.component';
import {CustomersHomeComponent} from './customers-home/customers-home.component';
import {GetAuthenticatedUserResolver} from '../../root-services/resolvers/get-authenticated-user.resolver';
import {GetStatesResolver} from '../../root-services/resolvers/get-states.resolver.';
import {CustomersFindComponent} from './customers-find/customers-find.component';
import {CustomerInfoComponent} from './customer-info/customer-info.component';

export const COMPONENT_DECLARATIONS: any[] = [
	CustomersCreateComponent,
	CustomersHomeComponent,
	CustomersFindComponent,
	CustomerInfoComponent
];

export const PROVIDERS_DECLARATIONS: any[] = [
];

export const ROUTES: Routes = [
	{
		path: '',
		component: CustomersHomeComponent,
		children: [
			{
				path: 'create',
				component: CustomersCreateComponent,
				resolve: {
					user: GetAuthenticatedUserResolver,
					states: GetStatesResolver
				}
			}
		]
	}
];
