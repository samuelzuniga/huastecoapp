import { Input, Component, OnInit } from '@angular/core';
import {IPerson, ICustomer, IAddress} from '../../../../../server/src-shared/interfaces';

@Component({
  selector: 'app-huasteco-customer-info',
  templateUrl: './customer-info.component.html',
  styleUrls: ['./customer-info.component.scss']
})
export class CustomerInfoComponent implements OnInit {
	@Input() customer: ICustomer;
	@Input() address: IAddress;

  constructor() { }

  ngOnInit() {
  }

}
