import {
	Component,
	OnInit,
	Output,
	EventEmitter,
	ViewChild
} from '@angular/core';
import {curry} from 'ramda';

import {IUserAuthenticated} from '../../../../../server/src-shared/interfaces';
import {ActivatedRoute} from '@angular/router';
import {IFindPersonResult} from '../../person/interfaces/find-person-result';
import {CustomersService} from '../../customers/services/customers.service';
import {FindPersonComponent, FindPersonConfiguration} from '../../person/find-person/find-person.component';

@Component({
	selector: 'app-huasteco-customers-find',
	templateUrl: './customers-find.component.html',
	styleUrls: ['./customers-find.component.scss']
})
export class CustomersFindComponent implements OnInit {
	@Output() selectCustomer: EventEmitter<IFindPersonResult> = new EventEmitter<IFindPersonResult>();
	@Output() clearCustomer: EventEmitter<void> = new EventEmitter<void>();
	@ViewChild('findPerson') findPerson: FindPersonComponent;
	user: IUserAuthenticated;
	findPersonConfiguration: FindPersonConfiguration;

	constructor(private route: ActivatedRoute,
				private customerService: CustomersService) {}

	ngOnInit() {
		this.user = this.route.snapshot.data.user;

		const getCustomers = curry(
			this.customerService.getCustomersFromCompany.bind(this.customerService)
		);

		this.findPersonConfiguration = {
			findAllMethod: getCustomers(this.user.company.id, 3),
			findOneMethod: this.customerService.getCustomer.bind(this.customerService)
		};

		this.findPerson.selectPerson
			.subscribe((result: IFindPersonResult) => this.selectCustomer.emit(result));

		this.findPerson.clearPerson
			.subscribe(_ => this.clearCustomer.next());
	}
}
