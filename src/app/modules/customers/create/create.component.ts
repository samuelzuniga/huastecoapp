import {Component, OnInit} from '@angular/core';
import {
	FormBuilder,
	FormGroup,
	Validators
} from '@angular/forms';
import {validateFormControls} from '../../shared/helpers/input-validation';
import {INTERNATIONAL_PHONE_NUMBER_PATTERN} from '../../shared/helpers/patterns';
import {GeoCodingService} from '../../map/services/geocoding.service';
import { ICity, ICustomer, IDistrict, IGeoCoderRequest, IState, IUserAuthenticated} from '../../../../../server/src-shared/interfaces';
import {CustomersService} from '../services/customers.service';
import {IClientError} from '../../shared/interfaces/client-error.interface';
import {FlashMessageService} from '../../core/services/flash-message.service';
import {ActivatedRoute, Router} from '@angular/router';
import {StatesService} from '../../../root-services/api/states.service';
import {CitiesService} from '../../../root-services/api/cities.service';

@Component({
	selector: 'app-huasteco-customers-create',
	templateUrl: './create.component.html',
	styleUrls: ['./create.component.scss']
})
export class CustomersCreateComponent implements OnInit {
	user: IUserAuthenticated;
	states: IState[];
	cities: ICity[];
	districts: IDistrict[];
	createCustomerForm: FormGroup;
	controlHasError: Function;
	latitude = null; // 51.678418;
	longitude = null; // 7.809007;
	requiredFieldMessage = 'El campo es obligatorio';
	// todo get this info from a service base on current user preferences
	defaultLocality = 'Poza Rica';
	defaultState = 'Ver';
	// todo show accents to client, remove them when send to api
	defaultCountry = 'México';
	isSubmitting: boolean;

	constructor(
		private route: ActivatedRoute,
		private formBuilder: FormBuilder,
		private flashMessageService: FlashMessageService,
		private geoCoderService: GeoCodingService,
		private customerService: CustomersService,
		private statesService: StatesService,
		private citiesService: CitiesService) {
	}

	// todo we need an extra number to define address number
	// maybe interior number
	// example  Hidalgo 5 departament 1 colonia Independencia
	ngOnInit() {
		this.isSubmitting = false;
		this.user = this.route.snapshot.data.user;
		this.states = this.route.snapshot.data.states;
		this.cities = [];
		this.districts = [];
		this.createCustomerForm = this.formBuilder.group({
			firstName: ['', [Validators.required]],
			lastName: ['', [Validators.required]],
			street: ['', [Validators.required]],
			suburb: ['', [Validators.required]],
			pictureURL: [''],
			postalCode: ['', [Validators.minLength(5)]],
			number: ['', [Validators.required]],
			insideNumber: ['', []],
			phoneNumber: ['', [Validators.required, Validators.pattern(INTERNATIONAL_PHONE_NUMBER_PATTERN)]],
			locality: [this.defaultLocality, [Validators.required] ],
			country: [this.defaultCountry, [Validators.required]],
			state: [this.defaultState, [Validators.required]]
		});
		this.controlHasError = validateFormControls(this.createCustomerForm);
	}

	onSubmit() {
		const {firstName, lastName, phoneNumber, street, number, insideNumber, suburb, locality, country, postalCode, state, pictureURL} =
			this.createCustomerForm.value;

		const customer: ICustomer = {
			companyId: this.user.company.id,
			firstName,
			lastName,
			phoneNumber,
			pictureURL,
			addresses: [
				{
					street,
					number,
					insideNumber,
					suburb,
					locality,
					postalCode,
					state,
					country,
					coordinates : {
						latitude: this.latitude,
						longitude: this.longitude
					}
				}
			]
		};

		// todo remove harcoded id
		this.isSubmitting = true;

		this.customerService.createCustomer(customer)
			.subscribe((newCustomer: ICustomer) => {
				this.isSubmitting = false;
				this.flashMessageService.showSuccess('Cliente creado correctamente');
			}, (error: IClientError) => {
				this.isSubmitting = false;
				this.flashMessageService.showError(error.message);
			});
	}

	updateMap(): void {
		if (this.isAddressReady()) {
			this.findAddressOnMap();
			console.log('Updating map');
		}
	}

	isAddressReady(): boolean {
		const {street, number, suburb, locality, country, postalCode, state} = this.createCustomerForm.value;

		return Boolean(street && number && suburb && locality && country && postalCode && state);
	}

	private findAddressOnMap(): void {
		const {street, number, suburb, locality, country, postalCode, state} = this.createCustomerForm.value;

		const suburbRecord = this.districts.find((item) => item.id === suburb);
		const localityRecord = this.cities.find((item) => item.id === locality);
		const stateRecord = this.states.find((item) => item.id === state);

		const request: IGeoCoderRequest = {
			address: `${street} ${number},${suburbRecord.name},${localityRecord.name}, ${stateRecord.name}.,${country}`,
			locality: locality,
			country: country
		};

		this.geoCoderService.geocode(request)
			.subscribe((result) => {
				if (result && result.geometry && result.geometry.location) {
					const {lat, lng} = result.geometry.location;
					this.latitude = lat;
					this.longitude = lng;
				}
			}, (error) => {
				console.log('there is an error queryig the service');
				console.log(error);
			});
	}


	onSelectState(event) {
		console.log('The event is:');
		console.log(event);
		this.statesService.getCitiesFromState(event.value)
			.subscribe((cities: ICity[]) => {
				this.cities = cities;
			}, (error) => console.log('Error:', error))
	}

	onSelectLocality(event) {
		console.log(event);
		this.citiesService.getDistrictsFromCity(event.value)
			.subscribe((districts: IDistrict[]) => {
				this.districts = districts;
				console.log(this.districts);
		}, (error) => console.log('Error:', error));
	}

	onSelectDistrict(event) {
		// console.log(event);
		const found = this.districts.find((district) => district.id === event.value);

		console.log('District');
		console.log(found);
		if (found) {
			this.createCustomerForm.controls['postalCode'].setValue(found.postalCode);
		}
	}
}
