import {Injectable} from '@angular/core';
import {
	catchError,
	map
} from 'rxjs/operators';
import {
	Observable,
} from 'rxjs';
import {
	HttpClient
} from '@angular/common/http';

import {environment} from '../../../../environments/environment';
import {IClientError} from '../../shared/interfaces/client-error.interface';

import {
	ICustomer,
	IResourceCustomer
} from '../../../../../server/src-shared/interfaces/index';
import {addIdToAttributes, getHttpHeadersWithApiKeys} from '../../shared/helpers/http-helper';


import {mapToClientError} from '../../shared/helpers/http-error-handler';
import {IAddress, IData, IResourceCustomers} from '../../../../../server/src-shared/interfaces';

// TODO create a shared server/client headers
const httpOptions = {
	headers: getHttpHeadersWithApiKeys()
};


@Injectable({
	providedIn: 'root'
})
export class CustomersService {
	private entity = 'customers';

	constructor(private http: HttpClient) {}

	createCustomer(customer: ICustomer): Observable<ICustomer | IClientError> {
		const body = JSON.stringify(customer);
		const url = `${environment.servicesURL}/${this.entity}`;

		return this.http.post<IResourceCustomer | IClientError>(url, body, httpOptions)
			.pipe(
				map((resource: IResourceCustomer) => resource.data.attributes),
				catchError(mapToClientError)
			);
	}

	getCustomersFromCompany(companyId: string | number, limit: number, findBy: string,  search = ''): Observable<ICustomer[] | IClientError> {
		let url = `${environment.servicesURL}/companies/${companyId}/customers?` +
			(search ? `findBy=${findBy}&search=${search}` : '');

		url += `&limit=${limit}`;

		return this.http.get<IResourceCustomers | IClientError>(url, httpOptions)
			.pipe(
				map((resource: IResourceCustomers) => {
					return resource.data.map((city: IData<ICustomer>) => addIdToAttributes(city));
				}),
				catchError(mapToClientError)
			);
	}

	getCustomer(id: string): Observable<ICustomer | IClientError> {
		const url = `${environment.servicesURL}/${this.entity}/${id}`;

		return this.http.get<IResourceCustomer | IClientError>(url, httpOptions)
			.pipe(
				map((resource: IResourceCustomer) => {
					resource.data.attributes.addresses.forEach((address: IAddress) => {
						address['id'] = address['_id'];
					});
					return resource.data.attributes;
				}),
				catchError(mapToClientError)
			);
	}
}
