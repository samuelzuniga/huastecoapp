import {Component, OnInit} from '@angular/core';

@Component({
	selector: 'app-huasteco-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
	title = 'huastecoApp';

	constructor() {
	}

	ngOnInit() {
	}
}
