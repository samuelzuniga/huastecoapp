import {Component, OnInit} from '@angular/core';
import {BreakpointObserver, Breakpoints, BreakpointState} from '@angular/cdk/layout';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {MainMenuService} from '../services/main-menu.service';
import {IMenuItem} from '../../shared/interfaces/main-menu.interface';

@Component({
	selector: 'app-huasteco-main-menu',
	templateUrl: './main-menu.component.html',
	styleUrls: ['./main-menu.component.scss'],
})
export class MainMenuComponent implements OnInit {
	menu: IMenuItem[] = [];
	isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
		.pipe(
			map(result => result.matches)
		);

	constructor(
		private breakpointObserver: BreakpointObserver,
		private mainMenuService: MainMenuService) {

		this.mainMenuService.getAllState()
			.subscribe((menu: IMenuItem[]) => this.menu = menu);
	}

	ngOnInit() {
	}
}
