import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FlexLayoutModule} from '@angular/flex-layout';

import {AppRoutingModule} from '../../app-routing.module';
import {COMPONENT_DECLARATIONS, PROVIDERS_DECLARATIONS} from './core.common';
import {MaterialModule} from '../material/material.module';
import {SuccessNotificationComponent} from './success-notification/success-notification.component';
import {ErrorNotificationComponent} from './error-notification/error-notification.component';

@NgModule({
	imports: [
		CommonModule,
		AppRoutingModule,
		FlexLayoutModule,
		MaterialModule
	],
	declarations: [
		...COMPONENT_DECLARATIONS
	],
	exports: [
		AppRoutingModule,
		...COMPONENT_DECLARATIONS
	],
	providers: [
		...PROVIDERS_DECLARATIONS
	],
	entryComponents: [SuccessNotificationComponent, ErrorNotificationComponent],
})
export class CoreModule {
}
