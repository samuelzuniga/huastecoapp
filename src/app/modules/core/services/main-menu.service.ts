import {Injectable} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {ACTION_FILTER_MENU} from '../../../store/actions';

import {AuthService} from '../../../root-services/api/auth.service';
import {IMenuItem} from '../../shared/interfaces/main-menu.interface';
import {AppState} from '../../../store/reducers';
import {Observable} from 'rxjs';
import {IUserAuthenticated} from '../../../../../server/src-shared/interfaces';


const ALL = '*';
const MENU: IMenuItem[] = [
	{
		label: 'Inicio',
		paths: ['/'],
		groups: [ALL]
	},
	{
		label: 'Login',
		paths: ['/auth', 'login'],
		groups: [ALL]
	},
/*	{
		label: 'Change password',
		paths: ['/auth', 'set-own-password'],
		groups: [ALL]
	}, */
	{
		label: 'Create User',
		paths: ['/users', 'create'],
		groups: ['Administradores']
	},
	{
		label: 'Create Customer',
		paths: ['/customers', 'create'],
		groups: ['Operadores']
	},
	{
		label: 'Create Dealer',
		paths: ['/dealers', 'create'],
		groups: ['Operadores']
	},
	{
		label: 'Orders Create',
		paths: ['/orders', 'create'],
		groups: ['Operadores']
	}
];

@Injectable()
export class MainMenuService {

	constructor(
		private store: Store<AppState>,
		private authService: AuthService
	) {

		this.store.dispatch({ type: ACTION_FILTER_MENU, payload: this.filterMenu(ALL)});

		this.authService.getAllState()
			.subscribe((user: IUserAuthenticated) => {
				// TODO filter by multiple groups, right now we are filtering by te first one found
				const group = user.groups.pop() || ALL;
				this.store.dispatch({ type: ACTION_FILTER_MENU, payload: this.filterMenu(group)});
			});
	}

	private filterMenu(group: string): IMenuItem[] {
		return MENU
			.filter((menu: IMenuItem) => menu.groups.indexOf(ALL) >= 0 || menu.groups.indexOf(group) >= 0)
			.map((menu: IMenuItem) => ({label: menu.label, paths: menu.paths}));
	}

	getAllState(): Observable<IMenuItem[]> {
		return this.store.pipe(select('mainMenuReducer'));
	}


}
