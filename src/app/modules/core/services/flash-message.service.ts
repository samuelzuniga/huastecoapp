import {Injectable} from '@angular/core';
import {
	MatSnackBar,
	MatSnackBarRef
} from '@angular/material';
import {ErrorNotificationComponent} from '../error-notification/error-notification.component';
import {SuccessNotificationComponent} from '../success-notification/success-notification.component';

@Injectable()
export class FlashMessageService {
	private snackBarRef: MatSnackBarRef<SuccessNotificationComponent | ErrorNotificationComponent>;
	constructor(private snackBar: MatSnackBar) {
	}

	showSuccess(message: string, duration: number = 3000): void {
		this.snackBarRef = this.snackBar.openFromComponent(SuccessNotificationComponent, { data: message, duration: duration });
	}
	showError(message: string, duration: number = 3000): void {
		this.snackBarRef = this.snackBar.openFromComponent(ErrorNotificationComponent, { data: message, duration: duration });
	}
}
