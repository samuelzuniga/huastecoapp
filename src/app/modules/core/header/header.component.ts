import {
	Component,
	OnInit
} from '@angular/core';
import {IUserAuthenticated} from '../../../../../server/src-shared/interfaces';
import {AuthService} from '../../../root-services/api/auth.service';

@Component({
	selector: 'app-huasteco-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
	user: IUserAuthenticated;

	constructor(private authService: AuthService) {}

	ngOnInit() {
		this.authService.getAllState()
			.subscribe((user: IUserAuthenticated) => this.user = user);
	}

}
