import { Routes } from '@angular/router';
import {HeaderComponent} from './header/header.component';
import {HomeComponent} from './home/home.component';
import {MainMenuComponent} from './main-menu/main-menu.component';
import {MainMenuService} from './services/main-menu.service';
import {FlashMessageService} from './services/flash-message.service';
import {SuccessNotificationComponent} from './success-notification/success-notification.component';
import {ErrorNotificationComponent} from './error-notification/error-notification.component';

export const COMPONENT_DECLARATIONS: any[] = [
	HeaderComponent,
	HomeComponent,
	MainMenuComponent,
	SuccessNotificationComponent,
	ErrorNotificationComponent
];

export const PROVIDERS_DECLARATIONS: any[] = [
	MainMenuService,
	FlashMessageService
];

export const ROUTES: Routes = [
];
