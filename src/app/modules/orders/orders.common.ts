import { Routes } from '@angular/router';
import {OrdersCreateComponent} from './create/create.component';
import {OrdersCreateSelectCustomerComponent} from './create-select-customer/create-select-customer.component';
import {OrdersHomeComponent} from './orders-home/orders-home.component';
import {GetCompanyProductsResolver} from '../../root-services/resolvers/get-company-products.resolver';
import {GetAuthenticatedUserResolver} from '../../root-services/resolvers/get-authenticated-user.resolver';
import {OrdersService} from './services/orders.service';
import {OrdersCreateSelectProductsComponent} from './create-select-products/create-select-products.component';
import {OrdersCreateShoppingCartComponent} from './create-shopping-cart/create-shopping-cart.component';
import {OrdersCreateConfirmComponent} from './create-confirm-order/create-confirm-order.component';

export const COMPONENT_DECLARATIONS: any[] = [
	OrdersCreateComponent,
	OrdersCreateSelectCustomerComponent,
	OrdersCreateSelectProductsComponent,
	OrdersCreateShoppingCartComponent,
	OrdersCreateConfirmComponent,
	OrdersHomeComponent
];

export const PROVIDERS_DECLARATIONS: any[] = [
	OrdersService
];

export const ROUTES: Routes = [
	{
		path: '',
		component: OrdersHomeComponent,
		children: [
			{
				path: 'create',
				component: OrdersCreateComponent,
				resolve: {
					products: GetCompanyProductsResolver,
					user: GetAuthenticatedUserResolver
				}
			}
		]
	},
];
