import {
	Component,
	OnDestroy,
	OnInit,
	ViewChild
} from '@angular/core';

import {
	FormBuilder,
	FormGroup,
	Validators
} from '@angular/forms';

import {
	MatDialog,
	MatStepper
} from '@angular/material';

import {
	BehaviorSubject,
	Observable,
	Subject,
	zip
} from 'rxjs';

import {
	filter,
	map,
	scan,
	switchMap,
	takeUntil,
	tap
} from 'rxjs/operators';

import {pathOr} from 'ramda';

import {
	IAddress,
	ICustomer,
	IPerson,
	IProduct,
	IUserAuthenticated
} from '../../../../../server/src-shared/interfaces';

import {ActivatedRoute} from '@angular/router';
import {CustomersService} from '../../customers/services/customers.service';
import {AddEditProductComponent} from '../../products/add-edit-product/add-edit-product.component';
import {IProductOrder} from '../../../../../server/src-shared/interfaces';
import {OrdersService} from '../services/orders.service';
import {CustomersFindComponent} from '../../customers/customers-find/customers-find.component';

interface IComponentState {
	order?: IProductOrder[];
}

@Component({
	selector: 'app-huasteco-create-shopping-cart',
	template: `
		<div class="huasteco">
			<app-huasteco-products-order-list
				[order]="state.order"
				[hasEdit]="true"
				[hasDelete]="true"
				(removeProduct)="onRemoveProductFromOrder($event)"
				(editProduct)="onEditProduct($event)">
			</app-huasteco-products-order-list>
		</div>
	`,
	styleUrls: ['./create-shopping-cart.component.scss']
})
export class OrdersCreateShoppingCartComponent implements OnInit, OnDestroy {
	order: IProductOrder[] = [];
	state: IComponentState;
	changeState: BehaviorSubject<IComponentState>;
	destroySubject: Subject<any>;

	constructor(private route: ActivatedRoute,
				private formBuilder: FormBuilder,
				private dialog: MatDialog,
				private ordersService: OrdersService) {
	}

	ngOnInit() {
		this.destroySubject = new Subject<any>();
		this.changeState = new BehaviorSubject<IComponentState>(this.getInitialState());

		this.changeState
			.pipe(
				takeUntil(this.destroySubject),
				scan((accumulator, current) =>
					Object.assign({}, accumulator, current), {})
			)
			.subscribe((state: IComponentState) => {
				this.state = state;
			});

		this.ordersService.getOrderState()
			.subscribe((state: IComponentState) => {
				this.changeState.next(state);
			});
	}

	ngOnDestroy() {
		this.destroySubject.next();
	}

	private getInitialState(): IComponentState {
		return {
			order: []
		};
	}

	onEditProduct(productOrder: IProductOrder) {
		const dialogRef =  this.dialog.open(AddEditProductComponent, {
			data: productOrder
		});

		this.editProduct(dialogRef.afterClosed());
	}

	editProduct(dialogClose$: Observable<any>) {
		dialogClose$
			.pipe(
				filter((productOrder: IProductOrder) =>  productOrder.quantity > 0 )
			)
			.subscribe((results: any) => {
				this.ordersService.editProduct(results);
			});
	}

	onRemoveProductFromOrder(productOrder: IProductOrder) {
		this.ordersService.removeProduct(productOrder);
	}
}
