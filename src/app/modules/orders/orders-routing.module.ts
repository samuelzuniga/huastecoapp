import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {ROUTES} from './orders.common';

@NgModule({
	imports: [RouterModule.forChild(ROUTES)],
	exports: [RouterModule]
})
export class OrdersRoutingModule {
}
