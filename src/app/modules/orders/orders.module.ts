import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FlexLayoutModule} from '@angular/flex-layout';
import {
	FormsModule,
	ReactiveFormsModule
} from '@angular/forms';

import {COMPONENT_DECLARATIONS, PROVIDERS_DECLARATIONS} from './orders.common';
import {OrdersRoutingModule} from './orders-routing.module';
import {MaterialModule} from '../material/material.module';
import {SharedModule} from '../shared/shared.module';
import {CustomersModule} from '../customers/customers.module';
import {ProductsModule} from '../products/products.module';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		FlexLayoutModule,
		MaterialModule,
		SharedModule,
		OrdersRoutingModule,
		CustomersModule,
		ProductsModule
	],
	declarations: [
		...COMPONENT_DECLARATIONS
	],
	providers: [
		...PROVIDERS_DECLARATIONS
	]
})
export class OrdersModule {
}
