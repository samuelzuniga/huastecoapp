import {
	Component,
	OnDestroy,
	OnInit
} from '@angular/core';

import {
	MatDialog
} from '@angular/material';

import {
	Subject,
	Observable,
	zip
} from 'rxjs';

import {
	filter,
	scan,
	switchMap,
	takeUntil
} from 'rxjs/operators';

import {pathOr} from 'ramda';

import {
	IProduct,
	IUserAuthenticated
} from '../../../../../server/src-shared/interfaces';

import {ActivatedRoute} from '@angular/router';
import {AddEditProductComponent} from '../../products/add-edit-product/add-edit-product.component';
import {IProductOrder} from '../../../../../server/src-shared/interfaces';
import {OrdersService} from '../services/orders.service';

interface IComponentState {
	products?: IProduct[];
}

@Component({
	selector: 'app-huasteco-orders-create-select-products',
	template: `
		<div class="huasteco">
			<app-huasteco-products-list
					[products]="state.products"
					[hasAddCommand]="true"
					(addProduct)="onAddProduct($event)">
			</app-huasteco-products-list>
		</div>
	`,
	styleUrls: ['./create-select-products.component.scss']
})
export class OrdersCreateSelectProductsComponent implements OnInit, OnDestroy {
	products: IProduct[] = [];
	user: IUserAuthenticated;
	state: IComponentState;
	changeState: Subject<IComponentState>;
	destroySubject: Subject<any>;

	constructor(private route: ActivatedRoute,
				private dialog: MatDialog,
				private ordersService: OrdersService) {
	}

	ngOnInit() {
		this.state = this.getInitialState();
		this.destroySubject = new Subject<any>();
		this.changeState = new Subject<IComponentState>();

		this.changeState
			.pipe(
				takeUntil(this.destroySubject),
				scan((accumulator, current) => Object.assign({}, accumulator, current), {})
			)
			.subscribe((state: IComponentState) => {
				this.state = state;
			});
	}

	ngOnDestroy() {
		this.destroySubject.next();
	}

	private getInitialState(): IComponentState {
		return {
			products: pathOr([], ['snapshot', 'data', 'products'], this.route)
		};
	}

	onAddProduct(product: IProduct) {
		const dialogRef =  this.dialog.open(AddEditProductComponent, {
			data: {product, quantity: 1}
		});

		this.addEditProduct(dialogRef.afterClosed());
	}

	addEditProduct(dialogClose$: Observable<any>) {
		dialogClose$
			.pipe(
				filter((productOrder: IProductOrder) =>  productOrder.quantity > 0 )
			)
			.subscribe((order: IProductOrder) => {
				this.ordersService.addProduct(order);
			});
	}
}
