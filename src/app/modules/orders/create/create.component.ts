import {
	Component,
	OnDestroy,
	OnInit,
	ViewChild
} from '@angular/core';

import {
	FormBuilder,
	FormGroup,
	Validators
} from '@angular/forms';

import {
	MatDialog,
	MatStepper
} from '@angular/material';

import {
	BehaviorSubject,
	Observable,
	Subject,
	zip
} from 'rxjs';

import {
	filter,
	map,
	scan,
	switchMap,
	takeUntil,
	tap
} from 'rxjs/operators';

import {pathOr} from 'ramda';

import {
	IAddress,
	ICustomer,
	IPerson,
	IProduct,
	IUserAuthenticated
} from '../../../../../server/src-shared/interfaces';

import {ActivatedRoute} from '@angular/router';
import {CustomersService} from '../../customers/services/customers.service';
import {AddEditProductComponent} from '../../products/add-edit-product/add-edit-product.component';
import {IProductOrder} from '../../../../../server/src-shared/interfaces';
import {OrdersService} from '../services/orders.service';
import {CustomersFindComponent} from '../../customers/customers-find/customers-find.component';

interface IComponentState {
	products?: IProduct[];
	customer?: IPerson;
	address?: IAddress;
	user?: IUserAuthenticated;
}

@Component({
	selector: 'app-huasteco-create-order',
	templateUrl: './create.component.html',
	styleUrls: ['./create.component.scss']
})
export class OrdersCreateComponent implements OnInit, OnDestroy {
	@ViewChild('stepper') stepper: MatStepper;
	@ViewChild('customersFind') customersFind: CustomersFindComponent;
	order: IProductOrder[] = [];
	customers: IPerson[] = [];
	user: IUserAuthenticated;
	search: string;
	isLinear = true;
	state: IComponentState;
	changeState: BehaviorSubject<IComponentState>;
	destroySubject: Subject<any>;

	constructor(private route: ActivatedRoute,
				private ordersService: OrdersService) {
	}

	ngOnInit() {
		this.destroySubject = new Subject<any>();
		this.changeState = new BehaviorSubject<IComponentState>(this.getInitialState())

		this.changeState
			.pipe(
				takeUntil(this.destroySubject),
				scan((accumulator, current) => Object.assign({}, accumulator, current), {})
			)
			.subscribe((state: IComponentState) => {
				this.state = state;
			});
	}

	ngOnDestroy() {
		this.destroySubject.next();
	}

	private getInitialState(): IComponentState {
		return {
			address: null,
			customer: null,
			products: pathOr([], ['snapshot', 'data', 'products'], this.route),
			user: pathOr(null, ['snapshot', 'data', 'user'], this.route)
		};
	}

	onSubmit() {
		console.log('On submit');
	}

	onSelectCustomer(selection: any) {
		this.changeState.next(selection);
	}

	onClearCustomer() {
		this.changeState.next({customer: null, address: null});
	}

}
