import {
	Component,
	EventEmitter,
	OnDestroy,
	Output,
	OnInit,
	ViewChild
} from '@angular/core';

import {
	BehaviorSubject,
	Subject
} from 'rxjs';

import {
	map,
	scan,
	takeUntil,
	tap
} from 'rxjs/operators';

import {pathOr} from 'ramda';

import {
	IAddress,
	IPerson
} from '../../../../../server/src-shared/interfaces';

import {ActivatedRoute} from '@angular/router';
import {CustomersFindComponent} from '../../customers/customers-find/customers-find.component';
import {IFindPersonResult} from '../../person/interfaces/find-person-result';

export interface ICreateOrderSelectCustomerState {
	customer?: IPerson;
	address?: IAddress;
}

@Component({
	selector: 'app-huasteco-orders-create-select-customer',
	template: `
		<div class="huasteco">
			<app-huasteco-card>
				<app-huasteco-customers-find #customersFind></app-huasteco-customers-find>
			</app-huasteco-card>
		</div>
	`,
	styleUrls: ['./create-select-customer.component.scss']
})
export class OrdersCreateSelectCustomerComponent implements OnInit, OnDestroy {
	@ViewChild('customersFind') customersFind: CustomersFindComponent;
	@Output() selectCustomer: EventEmitter<ICreateOrderSelectCustomerState> = new EventEmitter<ICreateOrderSelectCustomerState>();
	@Output() clearCustomer: EventEmitter<void> = new EventEmitter<void>();
	state: ICreateOrderSelectCustomerState;
	changeState: BehaviorSubject<ICreateOrderSelectCustomerState>;
	destroySubject: Subject<any>;

	constructor(private route: ActivatedRoute) {
	}

	ngOnInit() {
		this.destroySubject = new Subject<any>();
		this.setupStateChanges();
		this.setupCustomersFind();
	}

	ngOnDestroy() {
		this.destroySubject.next();
	}

	setupStateChanges() {
		this.changeState = new BehaviorSubject<ICreateOrderSelectCustomerState>(this.getInitialState())

		this.changeState
			.pipe(
				takeUntil(this.destroySubject),
				scan((accumulator, current) => Object.assign({}, accumulator, current), {})
			)
			.subscribe((state: ICreateOrderSelectCustomerState) => {
				this.state = state;
			});
	}

	setupCustomersFind() {
		// Selecting a customer target
		this.customersFind.selectCustomer
			.pipe(
				takeUntil(this.destroySubject),
				map((result: IFindPersonResult) => ({customer: result.person, address: result.address})),
				tap(_ => console.log('Selecting a customer'))
			)
			.subscribe((result: ICreateOrderSelectCustomerState) => {
				this.changeState.next({...result});
				this.selectCustomer.emit(result);
			});

		// on clear customer
		this.customersFind.clearCustomer
			.pipe(
				takeUntil(this.destroySubject),
				tap(_ => console.log('Clearing a customer'))
			)
			.subscribe(_ => {
				this.changeState.next({customer: null, address: null});
				this.clearCustomer.emit();
			});
	}

	getInitialState(): ICreateOrderSelectCustomerState {
		return {
			address: null,
			customer: null,
		};
	}
}
