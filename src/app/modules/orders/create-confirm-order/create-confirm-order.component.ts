import {
	Component,
	Input,
	OnInit,
} from '@angular/core';

import {pathOr} from 'ramda';

import {
	IAddress,
	ICustomer
} from '../../../../../server/src-shared/interfaces';

import {ActivatedRoute} from '@angular/router';
import {IProductOrder} from '../../../../../server/src-shared/interfaces';
import {OrdersService} from '../services/orders.service';
import {IOrderState} from '../../../store/reducers/order.reducer';

@Component({
	selector: 'app-huasteco-create-confirm-order',
	templateUrl: './create-confirm-order.component.html',
	styleUrls: ['./create-confirm-order.component.scss']
})
export class OrdersCreateConfirmComponent implements OnInit {
	@Input() customer: ICustomer;
	@Input() address: IAddress;

	order: IProductOrder[] = [];

	constructor(private route: ActivatedRoute,
				private ordersService: OrdersService) {
	}

	ngOnInit() {
		this.ordersService.getOrderState()
			.subscribe((orderState: IOrderState) => {
				this.order = orderState.order;
			})
	}

	onSubmit() {
		console.log('On submit');
	}
}
