import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {Store, select} from '@ngrx/store';
import {AppState} from '../../../store/reducers';
import {ACTION_ORDER_ADD_ITEM, ACTION_ORDER_EDIT_ITEM, ACTION_ORDER_REMOVE_ITEM} from '../../../store/actions';

import {IProductOrder} from '../../../../../server/src-shared/interfaces';

export interface IOrdersState {
	order: IProductOrder[];
}

@Injectable()
export class OrdersService {
	constructor(private store: Store<AppState>) {
	}

	getOrderState() {
		return this.store.pipe(select('orderReducer'));
	}

	removeProduct(productOrder: IProductOrder): void {
		this.store.dispatch({type: ACTION_ORDER_REMOVE_ITEM, payload: productOrder })
	}

	addProduct(newProductOrder: IProductOrder): void  {
		const productOrder = new ProductOrder(newProductOrder);
		this.store.dispatch({type: ACTION_ORDER_ADD_ITEM, payload: productOrder.getValue()});
	}

	editProduct(editedProductOrder: IProductOrder): void  {
		this.store.dispatch({type: ACTION_ORDER_EDIT_ITEM, payload: editedProductOrder});
	}
}


class ProductOrder {
	private productOrder: IProductOrder;

	constructor(productOrder: IProductOrder) {
		this.productOrder = productOrder;
		this.setSubtotal();
	}

	setSubtotal() {
		this.productOrder.subtotal = this.productOrder.quantity * this.productOrder.product.unitaryPrice;
	}

	setQuantity(quantity: number) {
		this.productOrder.quantity =  quantity;
		this.setSubtotal();
	}

	getValue(): IProductOrder {
		return this.productOrder;
	}

}
