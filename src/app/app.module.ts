import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {StoreModule} from '@ngrx/store';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {reducers} from './store/reducers';
import {CoreModule} from './modules/core/core.module';
import {environment} from '../environments/environment';
import {AgmCoreModule} from '@agm/core';
import {MapModule} from './modules/map/map.module';
import {httpInterceptorProviders} from './http-interceptors';
import { ProductsModule } from './modules/products/products.module';
// import {SuccessNotificationComponent} from './modules/shared/success-notification/success-notification.component';
// import {ErrorNotificationComponent} from './modules/shared/error-notification/error-notification.component';

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		HttpClientModule,
		StoreModule.forRoot(reducers, {}),
		AgmCoreModule.forRoot({
			apiKey: environment.googleAPIKey
		}),
		AppRoutingModule,
		CoreModule,
		MapModule,
		ProductsModule
	],
	providers: [httpInterceptorProviders],
	bootstrap: [AppComponent]
})
export class AppModule {
}
