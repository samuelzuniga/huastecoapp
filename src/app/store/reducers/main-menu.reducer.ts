import {
	ACTION_FILTER_MENU
} from '../actions';

import {IMenuItem} from '../../modules/shared/interfaces/main-menu.interface';


const initialState: IMenuItem[] = [];

export function mainMenuReducer(state = initialState, action): IMenuItem[] {
	const {payload} = action;

	switch (action.type) {
		case ACTION_FILTER_MENU:
			return [...payload];

	}

	return state;
}