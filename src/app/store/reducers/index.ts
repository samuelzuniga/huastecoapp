import {userReducer} from './user.reducer';
import {mainMenuReducer} from './main-menu.reducer';
import {orderReducer} from './order.reducer';
import {IUserAuthenticated} from '../../../../server/src-shared/interfaces';
import {ActionReducerMap} from '@ngrx/store';
import {IMenuItem} from '../../modules/shared/interfaces/main-menu.interface';
import {IOrdersState} from '../../modules/orders/services/orders.service';

export interface AppState {
	orderReducer: IOrdersState;
	userReducer: IUserAuthenticated;
	mainMenuReducer: IMenuItem[];
}

export const reducers: ActionReducerMap<AppState> = {
	userReducer,
	mainMenuReducer,
	orderReducer
};
