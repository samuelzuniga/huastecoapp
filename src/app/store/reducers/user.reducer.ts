import {
	ACTION_USER_LOGOUT,
	ACTION_USER_LOGIN
} from '../actions';

import {
	IUserAuthenticated
} from '../../../../server/src-shared/interfaces';
import {getEmptyUserAuthenticated} from '../../../../server/src/shared/cognito-helper';


const initialState: IUserAuthenticated = getEmptyUserAuthenticated();

export function userReducer(state = initialState, action): IUserAuthenticated {
	const {payload} = action;

	switch (action.type) {
		case ACTION_USER_LOGOUT:
			return {
				...state,
				isAuthenticated: false
			};

		case ACTION_USER_LOGIN:
			return {
				...state,
				...payload,
				isAuthenticated: true
			};
	}

	return state;
}