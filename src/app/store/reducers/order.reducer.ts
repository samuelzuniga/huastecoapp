import {
	ACTION_ORDER_ADD_ITEM,
	ACTION_ORDER_EDIT_ITEM,
	ACTION_ORDER_REMOVE_ITEM
} from '../actions';

import {
	 IProductOrder,
} from '../../../../server/src-shared/interfaces';

export interface IOrderState {
	order: IProductOrder[];
}

const initialState: IOrderState =  {
	order: []
};

export function orderReducer(state = initialState, action: {type: string, payload: IProductOrder}): IOrderState {
	const {payload} = action;

	switch (action.type) {
		case ACTION_ORDER_ADD_ITEM:
			return {
				order: [...state.order, payload],
			};

		case ACTION_ORDER_REMOVE_ITEM:
			return {
				order: state.order
					.filter((order: IProductOrder) => order.product.id !== payload.product.id)
			};

		case ACTION_ORDER_EDIT_ITEM:
			const found =  state.order.find((order: IProductOrder) => order.product.id === payload.product.id)
			found.quantity = payload.quantity;
			found.subtotal = payload.quantity * payload.product.unitaryPrice;
			const orders = state.order
				.filter((order: IProductOrder) => order.product.id !== payload.product.id);

			return {
				order: [...orders, found]
			};
	}

	return state;
}