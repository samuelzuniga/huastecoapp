import { Injectable } from '@angular/core';
import {
	 HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';
import {environment} from '../../environments/environment';

import {AuthService} from '../root-services/api/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

	constructor(private auth: AuthService) {}

	intercept(req: HttpRequest<any>, next: HttpHandler) {
		if (req.headers.get(environment.headers.huastecoIgnoreHttpInterceptors)) {
			console.log('Ignoring http interceptor');
			return next.handle(req);
		}

		// Get the auth token from the service.
		const authToken = this.auth.getAuthToken();

		// Clone the request and set the new header in one step.
		const authReq = req.clone({ setHeaders: { Authorization: authToken } });

		// send cloned request with header to the next handler.
		return next.handle(authReq);
	}
}


