import {Routes} from '@angular/router';
import {HomeComponent} from './modules/core/home/home.component';
import {AuthGuardService} from './root-services/guards/authorization.guard';


export const routes: Routes = [
	{
		path: '',
		component: HomeComponent
	},
	{
		path: 'auth',
		loadChildren: '../app/modules/auth/auth.module#AuthModule'
	},
	{
		path: 'users',
		canActivate: [AuthGuardService],
		loadChildren: '../app/modules/users/users.module#UsersModule'
	},
	{
		path: 'customers',
		canActivate: [AuthGuardService],
		loadChildren: '../app/modules/customers/customers.module#CustomersModule'
	},
	{
		path: 'orders',
		canActivate: [AuthGuardService],
		loadChildren: '../app/modules/orders/orders.module#OrdersModule'
	},
	{
		path: 'dealers',
		canActivate: [AuthGuardService],
		loadChildren: '../app/modules/dealers/dealers.module#DealersModule'
	}
];
