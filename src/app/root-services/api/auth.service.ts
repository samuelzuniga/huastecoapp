import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {select, Store} from '@ngrx/store';
import {environment} from '../../../environments/environment';
import {IClientError} from '../../modules/shared/interfaces/client-error.interface';
import {mapToClientError} from '../../modules/shared/helpers/http-error-handler';
import {getHttpHeadersWithApiKeys} from '../../modules/shared/helpers/http-helper';

import {AppState} from '../../store/reducers';
import {
	IResourceUserAuthenticated,
	IUserAuthenticated,
	IUser
} from '../../../../server/src-shared/interfaces';
import {
	ACTION_USER_LOGIN
} from '../../store/actions';


const httpOptions = {
	headers: getHttpHeadersWithApiKeys()
};

// todo we need to split this services into  api and auth
@Injectable({
	providedIn: 'root'
})
export class AuthService {
	authToken: string;
	constructor(
		private http: HttpClient,
		private store: Store<AppState>
	) {
		this.authToken = '';
	}

	authenticateUser(user: IUser): Promise<IUserAuthenticated> {
		return new Promise((resolve, reject) => {
			this.login(user)
				.subscribe((userAuthenticated: IUserAuthenticated) => {
					this.authToken = userAuthenticated.idToken;
					this.store.dispatch({ type: ACTION_USER_LOGIN, payload: userAuthenticated });
					return resolve(userAuthenticated);
				}, (error: IClientError) => {
					return reject(error);
				});
		});
	}

	// todo getState?
	getAllState(): Observable<IUserAuthenticated> {
		return this.store.pipe(select('userReducer'));
	}

	getAuthToken(): string {
		return this.authToken;
	}

	private login(user: IUser): Observable<IUserAuthenticated | IClientError> {
		const body = JSON.stringify(user);
		const url = `${environment.servicesURL}/users/login`;

		return this.http.post<IResourceUserAuthenticated | IClientError>(url, body, httpOptions)
			.pipe(
				map((resource: IResourceUserAuthenticated) => resource.data.attributes),
				catchError(mapToClientError)
			);
	}

}
