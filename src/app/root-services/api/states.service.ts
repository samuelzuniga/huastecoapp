import {Injectable} from '@angular/core';
import {
	catchError,
	map
} from 'rxjs/operators';
import {
	Observable,
} from 'rxjs';
import {
	HttpClient
} from '@angular/common/http';

import {environment} from '../../../environments/environment';
import {IClientError} from '../../modules/shared/interfaces/client-error.interface';

import {
	IData, IState, IResourceStates, ICustomer, IResourceCustomers, ICity, IResourceCities
} from '../../../../server/src-shared/interfaces';

import {mapToClientError} from '../../modules/shared/helpers/http-error-handler';
import {getHttpHeadersWithApiKeys, addIdToAttributes} from '../../modules/shared/helpers/http-helper';

const httpOptions = {
	headers: getHttpHeadersWithApiKeys()
};

@Injectable({
	providedIn: 'root'
})
export class StatesService {
	private entity = 'states';

	constructor(
		private http: HttpClient,
	) {}

	getStates(): Observable<IState[] | IClientError> {
		const url = `${environment.servicesURL}/${this.entity}`;

		return this.http.get<IResourceStates | IClientError>(url, httpOptions)
			.pipe(
				map((resource: IResourceStates) => {
					return resource.data.map((state: IData<IState>) => addIdToAttributes(state));
				}),
				catchError(mapToClientError)
			);
	}

	getCitiesFromState(stateId: string, name = ''): Observable<ICity[] | IClientError> {
		const url = `${environment.servicesURL}/${this.entity}/${stateId}/cities` + (name ? `?name=${name}` : '');

		return this.http.get<IResourceCities | IClientError>(url, httpOptions)
			.pipe(
				map((resource: IResourceCities) => {
					return resource.data.map((city: IData<ICity>) => addIdToAttributes(city));
				}),
				catchError(mapToClientError)
			);
	}
}
