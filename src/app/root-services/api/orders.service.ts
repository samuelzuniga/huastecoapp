import {Injectable} from '@angular/core';
import {
	catchError,
	map
} from 'rxjs/operators';
import {
	Observable,
} from 'rxjs';
import {
	HttpClient
} from '@angular/common/http';

import {environment} from '../../../environments/environment';
import {IClientError} from '../../modules/shared/interfaces/client-error.interface';

import {
	IData,
	IResourceProducts,
	IProduct
} from '../../../../server/src-shared/interfaces/index';

import {mapToClientError} from '../../modules/shared/helpers/http-error-handler';
import {getHttpHeadersWithApiKeys} from '../../modules/shared/helpers/http-helper';


const httpOptions = {
	headers: getHttpHeadersWithApiKeys()
};

@Injectable({
	providedIn: 'root'
})
export class APIOrdersService {
	private entity = 'orders';

	constructor(
		private http: HttpClient,
	) {}

	// todo move it to the products service
	getProductsByCompany(idCompany: string | number): Observable<IProduct[] | IClientError> {
		const url = `${environment.servicesURL}/companies/${idCompany}/products`;

		return this.http.get<IResourceProducts | IClientError>(url, httpOptions)
			.pipe(
				map((resource: IResourceProducts) => {
					return resource.data.map((product: IData<IProduct>) => {
						return { id: product.id,  ...product.attributes };
					});
				}),
				catchError(mapToClientError)
			);
	}
}
