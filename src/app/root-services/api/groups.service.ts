import {Injectable} from '@angular/core';
import {
	catchError,
	map
} from 'rxjs/operators';
import {
	Observable,
} from 'rxjs';
import {
	HttpClient
} from '@angular/common/http';

import {environment} from '../../../environments/environment';
import {IClientError} from '../../modules/shared/interfaces/client-error.interface';

import {
	IGroup,
	IResourceGroups
} from '../../../../server/src-shared/interfaces';
import {getHttpHeadersWithApiKeys} from '../../modules/shared/helpers/http-helper';


import {mapToClientError} from '../../modules/shared/helpers/http-error-handler';

const httpOptions = {
	headers: getHttpHeadersWithApiKeys()
};


@Injectable({
	providedIn: 'root'
})
export class GroupsService {
	private entity = 'groups';

	constructor(
		private http: HttpClient,
	) {}

	getAll(): Observable<IGroup[] | IClientError> {
		const url = `${environment.servicesURL}/${this.entity}`;

		return this.http.get<IResourceGroups | IClientError>(url, httpOptions)
			.pipe(
				map((resource: IResourceGroups) => resource.data.map(group => group.attributes)),
				catchError(mapToClientError)
			);
	}
}
