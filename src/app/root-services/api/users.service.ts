import {Injectable} from '@angular/core';
import {
	catchError,
	map
} from 'rxjs/operators';
import {
	Observable,
} from 'rxjs';
import {
	HttpClient,
	HttpHeaders
} from '@angular/common/http';

import {environment} from '../../../environments/environment';
import {IClientError} from '../../modules/shared/interfaces/client-error.interface';
import {
	IPasswordChange,
	IUser,
	IUserTokens,
	IResourceUser
} from '../../../../server/src-shared/interfaces/index';


import {mapToClientError} from '../../modules/shared/helpers/http-error-handler';
import {getHttpHeadersWithApiKeys} from '../../modules/shared/helpers/http-helper';

const httpOptions = {
	headers: getHttpHeadersWithApiKeys()
};


@Injectable({
	providedIn: 'root'
})
export class UsersService {
	private entity = 'users';

	constructor(
		private http: HttpClient,
	) {

	}

	// todo refactor data type result
	forceChangePassword(credentials: IPasswordChange): Observable<IUserTokens | IClientError> {
		const body = JSON.stringify(credentials);
		const url = `${environment.servicesURL}/${this.entity}/force-change-password`;

		return this.http.post<IUserTokens | IClientError>(url, body, httpOptions)
			.pipe(
				catchError(mapToClientError)
			);
	}

	createUser(user: IUser): Observable<IUser | IClientError> {
		const body = JSON.stringify(user);
		const url = `${environment.servicesURL}/${this.entity}`;

		return this.http.post<IResourceUser | IClientError>(url, body, httpOptions)
			.pipe(
				map((resource: IResourceUser) => resource.data.attributes),
				catchError(mapToClientError)
			);
	}
}
