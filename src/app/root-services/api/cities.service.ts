import {Injectable} from '@angular/core';
import {
	catchError,
	map
} from 'rxjs/operators';
import {
	Observable,
} from 'rxjs';
import {
	HttpClient
} from '@angular/common/http';

import {environment} from '../../../environments/environment';
import {IClientError} from '../../modules/shared/interfaces/client-error.interface';

import {
	IData, IResourceCustomers, IResourceDistricts, IDistrict
} from '../../../../server/src-shared/interfaces';

import {mapToClientError} from '../../modules/shared/helpers/http-error-handler';
import {getHttpHeadersWithApiKeys, addIdToAttributes} from '../../modules/shared/helpers/http-helper';

const httpOptions = {
	headers: getHttpHeadersWithApiKeys()
};

@Injectable({
	providedIn: 'root'
})
export class CitiesService {
	private entity = 'cities';

	constructor(
		private http: HttpClient,
	) {}

	getDistrictsFromCity(cityId: string, name = ''): Observable<IDistrict[] | IClientError> {
		const url = `${environment.servicesURL}/${this.entity}/${cityId}/districts` + (name ? `?name=${name}` : '');

		return this.http.get<IResourceCustomers | IClientError>(url, httpOptions)
			.pipe(
				map((resource: IResourceDistricts) => {
					return resource.data.map((city: IData<IDistrict>) => addIdToAttributes(city));
				}),
				catchError(mapToClientError)
			);
	}
}
