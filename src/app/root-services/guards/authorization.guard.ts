import {Injectable} from '@angular/core';
import {
	ActivatedRouteSnapshot,
	CanActivate,
	Router,
	RouterStateSnapshot
} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from '../api/auth.service';
import {IUserAuthenticated} from '../../../../server/src-shared/interfaces/index';



@Injectable({
	providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
	userAuth: IUserAuthenticated;
	constructor(private router: Router,
				private authService: AuthService) {
		this.authService.getAllState()
			.subscribe((user: IUserAuthenticated) => this.userAuth = user);
	}

	canActivate(route: ActivatedRouteSnapshot,
				state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

		if (!this.userAuth.isAuthenticated) {
			this.router.navigate(['/auth', 'login']);
		}

		return Promise.resolve(this.userAuth.isAuthenticated);
	}

}
