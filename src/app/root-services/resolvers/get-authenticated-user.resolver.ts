import { Injectable } from '@angular/core';

import { Resolve } from '@angular/router';
import {AuthService} from '../api/auth.service';
import {IUserAuthenticated} from '../../../../server/src-shared/interfaces';

@Injectable({
	providedIn: 'root'
})
export class GetAuthenticatedUserResolver implements Resolve<any> {
	constructor(private authService: AuthService) {}

	resolve() {
		return new Promise((resolve, reject) => {
			return this.authService.getAllState()
				.subscribe((user: IUserAuthenticated) => resolve(user), error => reject(error));
		});
	}
}