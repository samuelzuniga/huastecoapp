import {Injectable} from '@angular/core';
import {Resolve} from '@angular/router';
import {switchMap} from 'rxjs/operators';

import {AuthService} from '../api/auth.service';
import {IProduct, IUserAuthenticated} from '../../../../server/src-shared/interfaces';
import {APIOrdersService} from '../api/orders.service';

@Injectable({
	providedIn: 'root'
})
export class GetCompanyProductsResolver implements Resolve<any> {
	constructor(
		private authService: AuthService,
		private apiOrdersService: APIOrdersService) {}

	resolve()  {
		return new Promise((resolve, reject) =>  {
			return this.authService.getAllState()
				.pipe(
					switchMap((user: IUserAuthenticated) => this.apiOrdersService.getProductsByCompany(user.company.id))
				).subscribe((products: IProduct[]) => resolve(products), err => reject(err));
		});
	}
}