import { Injectable } from '@angular/core';

import { Resolve } from '@angular/router';
import {GroupsService} from '../api/groups.service';

@Injectable({
	providedIn: 'root'
})
export class GetGroupsResolver implements Resolve<any> {
	constructor(private groupsService: GroupsService) {}

	resolve() {
		return this.groupsService.getAll();
	}
}