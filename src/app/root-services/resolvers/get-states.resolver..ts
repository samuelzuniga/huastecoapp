import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import {StatesService} from '../api/states.service';

@Injectable({
	providedIn: 'root'
})
export class GetStatesResolver implements Resolve<any> {
	constructor(private statesService: StatesService) {}

	resolve() {
		return this.statesService.getStates();
	}
}