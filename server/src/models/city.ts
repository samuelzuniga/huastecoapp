const mongoose = require('mongoose');

const Schema = mongoose.Schema;

export const CitySchema = new Schema({
	name: {
		type: String,
		required: true
	},
    longName: {
        type: String,
        required: true
    },
    stateId: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        default: new Date()
    },
});

export const CityModel = mongoose.model('City', CitySchema);
