import {AddressSchema} from './address';
import {CustomerSchema} from './customer';
import {ProductSchema} from './products';
import {UserSchema} from './user';

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

export const CompanySchema = new Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
	phoneNumber: {
		type: String,
		required: true
	},
	address: AddressSchema,
    created: {
        type: Date,
        default: new Date()
    },
	users: [UserSchema],
	customers: [CustomerSchema],
	products: [ProductSchema]
});

export const CompanyModel = mongoose.model('Company', CompanySchema);
