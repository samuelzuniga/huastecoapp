import {PersonSchema} from './person';
const extendSchema = require('mongoose-extend-schema');

const mongoose = require('mongoose');

export const DealerSchema =  extendSchema(PersonSchema, {
	companyId: {
		type: String,
		required: true
	},
	username: {
		type: String,
		required: true
	},
	pictureURL: {
		type: String,
		default: 'user.png'
	},
	email: {
		type: String,
		required: true
	},
	created: {
		type: Date,
		default: new Date()
	}
});

export const DealerModel = mongoose.model('Dealer', DealerSchema);


