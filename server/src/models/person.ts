const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// a person should only have one telephone
// because it could have some addresses
// _id: false,
export const PersonSchema = new Schema({
	firstName: {
		type: String,
		required: true,
		lowercase: true
	},
	lastName: {
		type: String,
		required: true,
		lowercase: true
	},
	phoneNumber: {
		type: String,
		required: true
	}
} );

export const PersonModel = mongoose.model('Person', PersonSchema);
