const mongoose = require('mongoose');
const Schema = mongoose.Schema;

export const UserSchema = new Schema({
	username: {
		type: String,
		required: true
	},
	created: {
		type: Date,
		default: new Date()
	}
});

export const UserModel = mongoose.model('User', UserSchema);