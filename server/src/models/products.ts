// TODO rename file and model to Product
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

export const ProductSchema = new Schema({
	name: {
		type: String,
		required: true
	},
	brand: {
		type: String,
		required: true
	},
	unitaryPrice: {
		type: Number,
		required: true
	}
});

export const ProductsModel = mongoose.model('Product', ProductSchema);
