import {CoordinatesSchema} from './coordinates';

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

export const AddressSchema = new Schema({
	street: {
		type: String,
		required: true
	},
	number: {
		type: String,
		required: true
	},
	insideNumber: {
		type: String,
		required: false
	},
	suburb: {
		type: String,
		required: true
	},
	postalCode: {
		type: Number,
		required: true
	},
	locality: {
		type: String,
		required: true
	},
	state: {
		type: String,
		required: true
	},
	country: {
		type: String,
		default: 'México',
		required: true
	},
	coordinates: {
		type: CoordinatesSchema,
		required: false
	}
});

export const AddressModel = mongoose.model('Address', AddressSchema);
