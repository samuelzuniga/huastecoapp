const mongoose = require('mongoose');

const Schema = mongoose.Schema;

export const CoordinatesSchema = new Schema({
	_id: false,
	latitude: {
		type: Number,
		required: false
	},
	longitude: {
		type: Number,
		required: false
	}
});

export const CoordinatesModel = mongoose.model('Coordinates', CoordinatesSchema);
