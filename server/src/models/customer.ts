import {PersonSchema} from './person';
import {AddressSchema} from './address';
const extendSchema = require('mongoose-extend-schema');


const mongoose = require('mongoose');

export const CustomerSchema = extendSchema(PersonSchema, {
	companyId: {
		type: String,
		required: true
	},
	addresses: [AddressSchema],
	created: {
		type: Date,
		default: new Date()
	}
}, {
	toObject: {
		virtuals: true
	},
	toJSON: {
		virtuals: true
	}
});

CustomerSchema
	.virtual('fullName')
	.get(function() {
		return this.firstName + ' ' + this.lastName;
	});


export const CustomerModel = mongoose.model('Customer', CustomerSchema);
