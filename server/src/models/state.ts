const mongoose = require('mongoose');

const Schema = mongoose.Schema;

export const StateSchema = new Schema({
	abbreviation: {
		type: String,
		required: true
	},
    name: {
        type: String,
        required: true
    },
    longName: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        default: new Date()
    },
});

export const StateModel = mongoose.model('State', StateSchema);
