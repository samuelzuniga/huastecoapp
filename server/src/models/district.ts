const mongoose = require('mongoose');

const Schema = mongoose.Schema;

export const DistrictSchema = new Schema({
	cityId: {
		type: String,
		required: true
	},
    name: {
        type: String,
        required: true
    },
    postalCode: {
        type: Number,
        required: true
    },
    created: {
        type: Date,
        default: new Date()
    },
});

export const DistrictModel = mongoose.model('District', DistrictSchema);
