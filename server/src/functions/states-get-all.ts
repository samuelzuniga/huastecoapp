import {APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda';
import {openDatabase} from '../shared/database';
import {mapToResourceStates} from '../shared/json-api';
import {IState, IError} from '../../src-shared/interfaces';
import {
	createErrorLambdaResponse, getSuccessLambdaResponse, ILambdaResponse
} from '../shared/lambda-response';
import {validateAuthToken} from '../shared/validate-token';
import {StateModel} from '../models/state';
import {
	createDatabaseError,
	createRuntimeError
} from '../../src-shared/error.factory';



export const handler: Handler = (event: APIGatewayEvent, context: Context, callback: Callback) => {
	context.callbackWaitsForEmptyEventLoop = false;
	const authorization = event.headers && event.headers.Authorization || '';

	validateAuthToken(authorization)
		.then(() => getStates())
		.then(mapToResourceStates)
		.then(getSuccessLambdaResponse)
		.then((response: ILambdaResponse) => callback(null, response))
		.catch((error: IError) => {
			error = error.isHandled ? error : createRuntimeError(error.message);

			return createErrorLambdaResponse(error)
				.then((response: any) => callback(null, response));
		});
};

function getStates(): Promise<IState[] | IError> {
	return new Promise((resolve, reject) => {
		openDatabase()
			.then(() => StateModel.find({}, (err, states) => {
				if (err) {
					return reject(createDatabaseError(err.message));
				}
				return resolve(states);
			}))
			.catch((error: IError) => reject(error));
	});
}

