import {APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda';
import {openDatabase} from '../shared/database';
import {mapToResourceDistricts} from '../shared/json-api';
import {IError, IDistrict} from '../../src-shared/interfaces';
import {
	createErrorLambdaResponse, getSuccessLambdaResponse, ILambdaResponse
} from '../shared/lambda-response';
import {validateAuthToken} from '../shared/validate-token';
import {
	createDatabaseError,
	createRuntimeError
} from '../../src-shared/error.factory';
import {DistrictModel} from '../models';

export const handler: Handler = (event: APIGatewayEvent, context: Context, callback: Callback) => {
	context.callbackWaitsForEmptyEventLoop = false;
	const {cityId} = event.pathParameters;
	const {name = ''} = event.queryStringParameters || {};
	const authorization = event.headers && event.headers.Authorization || '';

	validateAuthToken(authorization)
		.then(() => openDatabase())
		.then(() => getDistricts(cityId, name))
		.then(mapToResourceDistricts)
		.then(getSuccessLambdaResponse)
		.then((response: ILambdaResponse) => callback(null, response))
		.catch((error: IError) => {
			error = error.isHandled ? error : createRuntimeError(error.message);
			return createErrorLambdaResponse(error)
				.then((response: any) => callback(null, response));
		});
};


function getDistricts(cityId: string, name = ''): Promise<IDistrict[] | IError> {
	const filter = {
		cityId: cityId
	};

	if (name) {
		filter['name'] = { $regex: new RegExp(name, 'gi')  };
	}

	return new Promise((resolve, reject) => {
		return DistrictModel.find(filter, null, { sort: {name: 1} }, (error, data) => {
			if (error) {
				return reject(createDatabaseError(error.message));
			}
			return resolve(data);
		});
	});
}

