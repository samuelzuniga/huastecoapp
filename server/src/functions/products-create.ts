import {APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda';
import {CompanyModel} from '../models';
import {openDatabase} from '../shared/database';
import {mapToResourceProduct} from '../shared/json-api';
import {IProduct} from '../../src-shared/interfaces';
import {getCreatedLambdaResponse, getUnauthorizedLambdaResponse, ILambdaResponse} from '../shared/lambda-response';

export const handler: Handler = (event: APIGatewayEvent, context: Context, callback: Callback) => {
	context.callbackWaitsForEmptyEventLoop = false;
	const {companyId} = event.pathParameters;
	const body = event.body ? JSON.parse(event.body) : event;

	createProduct(companyId, body)
		.then(mapToResourceProduct)
		.then(getCreatedLambdaResponse)
		.then((response: ILambdaResponse) => callback(null, response))
		.catch((error) => {
			return getUnauthorizedLambdaResponse(error)
				.then((response: any) => callback(null, response));
		});
};

async function createProduct(companyId: string, product: IProduct): Promise<IProduct> {
	try {
		await openDatabase();
		const resource = await createProductOnDatabase(companyId, product);

		if (resource && resource.products && resource.products.length) {
			return Promise.resolve(resource.products.pop());
		}

		return Promise.reject(new Error('Cannot create product'));
	} catch (error) {
		return Promise.reject(new Error(error.message || error));
	}
}

function createProductOnDatabase(companyId: string, product: IProduct): Promise<any> {
	return CompanyModel.findByIdAndUpdate(companyId,
		{$push: {products: product}},
		{new: true, runValidators: true});
}




