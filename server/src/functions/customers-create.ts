import {APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda';
import {CustomerModel} from '../models';
import {openDatabase} from '../shared/database';
import {mapToResourceCustomer} from '../shared/json-api';
import {ICustomer, IError} from '../../src-shared/interfaces';
import {
	createErrorLambdaResponse, getCreatedLambdaResponse, ILambdaResponse
} from '../shared/lambda-response';
import {createDatabaseError, createRuntimeError} from '../../src-shared/error.factory';
import {validateAuthToken} from '../shared/validate-token';

export const handler: Handler = (event: APIGatewayEvent, context: Context, callback: Callback) => {
	context.callbackWaitsForEmptyEventLoop = false;
	const body = event.body ? JSON.parse(event.body) : event;
	const authorization = event.headers && event.headers.Authorization || '';

	validateAuthToken(authorization)
		.then(() => openDatabase())
		.then(() => createCustomer(body))
		.then(mapToResourceCustomer)
		.then(getCreatedLambdaResponse)
		.then((response: ILambdaResponse) => callback(null, response))
		.catch((error) => {
			return createErrorLambdaResponse(error.isHandled ? error : createRuntimeError(error.message))
				.then((response: any) => callback(null, response));
		});
};

function createCustomer(customer: any): Promise<ICustomer | IError> {
	return new Promise((resolve, reject) => {
		const newCustomer = new CustomerModel(customer);
		return newCustomer.save( (error, result) => {
					if (error) {
						return reject(createDatabaseError(error.message));
					}
					return resolve(result);
				});
	});
}
