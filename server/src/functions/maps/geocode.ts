import {APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda';
// global['fetch'] = require('node-fetch');

import {
	ILambdaResponse,
	getSuccessLambdaResponse,
	getUnauthorizedLambdaResponse
} from '../../shared/lambda-response';

const googleMapsClient = require('@google/maps').createClient({
	key: process.env.HUASTECO_GOOGLE_MAPS_API,
	Promise: Promise
});

export const handler: Handler = (event: APIGatewayEvent, context: Context, callback: Callback) => {
	context.callbackWaitsForEmptyEventLoop = false;
	const body = event.body ? JSON.parse(event.body) : event;

	googleMapsClient.geocode({address: body.address})
		.asPromise()
		.then(getSuccessLambdaResponse)
		.then((response: ILambdaResponse) => callback(null, response))
		.catch((error) => {
			return getUnauthorizedLambdaResponse(error)
				.then((response: any) => callback(null, response));
		});
};
