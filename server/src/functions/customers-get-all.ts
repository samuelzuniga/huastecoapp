import {APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda';
import {CustomerModel} from '../models';
import {openDatabase} from '../shared/database';
import {mapToResourceCustomers} from '../shared/json-api';
import {ICustomer} from '../../src-shared/interfaces';
import {
	createErrorLambdaResponse, getSuccessLambdaResponse, ILambdaResponse
} from '../shared/lambda-response';
import {validateAuthToken} from '../shared/validate-token';
import {IError} from '../../src-shared/interfaces/error';
import {
	createDatabaseError,
	createInvalidParameterError, createMissingParametersError,
	createRuntimeError
} from '../../src-shared/error.factory';
import {FindPersonType} from '../../src-shared/enums/person';

const FIELD_PHONE_NUMBER = 'phoneNumber';
const FIELD_NAME = 'name';

export const handler: Handler = (event: APIGatewayEvent, context: Context, callback: Callback) => {
	context.callbackWaitsForEmptyEventLoop = false;
	const {companyId} = event.pathParameters;
	const {findBy = '', search = '', limit = 10} = event.queryStringParameters || {};

	const authorization = event.headers && event.headers.Authorization || '';

	validateAuthToken(authorization)
		.then(() => validateSearchParameters(findBy, search))
		.then(() => openDatabase())
		.then(() => getCustomers(companyId, findBy, search, parseInt(limit, 10)))
		.then(mapToResourceCustomers)
		.then(getSuccessLambdaResponse)
		.then((response: ILambdaResponse) => callback(null, response))
		.catch((error: IError) => {
			error = error.isHandled ? error : createRuntimeError(error.message || 'unknown error');

			return createErrorLambdaResponse(error)
				.then((response: any) => callback(null, response));
		});
};

function validateSearchParameters(findBy: string, search: string): Promise<boolean | IError> {
	if (!search.trim()) {
		return Promise.reject(createMissingParametersError('You need to send "search" query parameter'));
	}

	if (![FindPersonType.phoneNumber.toString(), FindPersonType.name.toString()]
			.find((findType) => findType === findBy)) {
		return Promise.reject(createInvalidParameterError('Invalid findBy parameter'));
	}

	return Promise.resolve(true);
}


function getCustomers(companyId: string, findBy: string, search: string, limit: number): Promise<ICustomer[] | IError> {
	const filter = {
		companyId: companyId
	};

	if (search) {
		const reg = new RegExp(search, 'gi');
		filter['phoneNumber'] = { $regex: reg }
	}

	return new Promise((resolve, reject) => {
		return CustomerModel
			.find(filter)
			.limit(limit)
			.sort({name: 1})
			.select({_id: 1, firstName: 1, lastName: 1, phoneNumber: 1, fullName: 1})
			.exec((error, data) => {
				if (error) { return reject(createDatabaseError(error.message)); }

				return resolve(data);
			});
	});
}

