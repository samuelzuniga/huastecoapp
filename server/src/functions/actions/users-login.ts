import {APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda';
global['fetch'] = require('node-fetch');

import {login as cognitoLogin} from '../../shared/cognito';
import {openDatabase} from '../../shared/database';
import {mapToResourceUserAuthenticated} from '../../shared/json-api';
import {IError} from '../../../src-shared/interfaces';
import {
	ILambdaResponse,
	getSuccessLambdaResponse,
	createErrorLambdaResponse
} from '../../shared/lambda-response';
import {IUserAuthenticated} from '../../../src-shared/interfaces';
import {CompanyModel} from '../../models';
import {ICompany} from '../../../src-shared/interfaces/company';
import {createDatabaseError, createInvalidCredentials, createRuntimeError} from '../../../src-shared/error.factory';


export const handler: Handler = (event: APIGatewayEvent, context: Context, callback: Callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    const body = event.body ? JSON.parse(event.body) : event;

    cognitoLogin(body)
		.then((user) => {
			return openDatabase()
				.then(() => user);
		})
		.then((user: IUserAuthenticated) => {
			return Promise.all([Promise.resolve(user), findUserCompany(user.username)]);
		 })
		.then((result: any[]) => {
			const [user, company] = result;

			user.company = company;
			return user;
		})
		.then(mapToResourceUserAuthenticated)
		.then(getSuccessLambdaResponse)
        .then((response: ILambdaResponse) => callback(null, response))
        .catch((error) =>
			createErrorLambdaResponse(error.isHandled ? error : createRuntimeError(error.message))
				.then((response: any) => callback(null, response))
        );
};


function findUserCompany(username: string): Promise<ICompany | IError> {
	return new Promise((resolve, reject) => {
		return CompanyModel.findOne({'users.username': username}, (error, company) => {
			if (error) {
				return reject(createDatabaseError(error.message));
			}

			if (company) {
				const {id, description, phoneNumber, name} = company;
				return resolve(<ICompany>{id, name, description, phoneNumber});
			}

			return reject(createInvalidCredentials('User invalid'));
		});
	});
}
