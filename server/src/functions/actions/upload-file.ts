import {APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda';
const AWS = require('aws-sdk');
global['fetch'] = require('node-fetch');


export const handler: Handler = (event: APIGatewayEvent, context: Context, callback: Callback) => {
	const s3 = new AWS.S3();
	const params = JSON.parse(event.body);

	const s3Params = {
		Bucket: 'huasteco-upload-bucket',
		Key:  params.name,
		ContentType: params.type,
		ACL: 'public-read'
	};

	const uploadURL = s3.getSignedUrl('putObject', s3Params);

	callback(null, {
		statusCode: 200,
		headers: {
			'Access-Control-Allow-Origin': '*'
		},
		body: JSON.stringify({ uploadURL: uploadURL }),
	})
};


