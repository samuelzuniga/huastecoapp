import {APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda';
global['fetch'] = require('node-fetch');

import {forceChangePassword} from '../../shared/cognito';
import {mapToResourceUserAuthenticated} from '../../shared/json-api';
import {
	ILambdaResponse,
	getSuccessLambdaResponse,
	getBadRequestLambdaResponse
} from '../../shared/lambda-response';


export const handler: Handler = (event: APIGatewayEvent, context: Context, callback: Callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    const body = event.body ? JSON.parse(event.body) : event;

    forceChangePassword(body)
		.then(mapToResourceUserAuthenticated)
        .then(getSuccessLambdaResponse)
		.then((response: ILambdaResponse) => callback(null, response))
        .catch((error) => {
			return getBadRequestLambdaResponse(error)
				.then((response: any) => callback(null, response));
        });
};
