import {APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda';
import {openDatabase} from '../shared/database';
import {mapToResourceCities} from '../shared/json-api';
import {IError, ICustomer} from '../../src-shared/interfaces';
import {
	createErrorLambdaResponse, getSuccessLambdaResponse, ILambdaResponse
} from '../shared/lambda-response';
import {validateAuthToken} from '../shared/validate-token';
import {
	createDatabaseError,
	createRuntimeError
} from '../../src-shared/error.factory';
import {CityModel} from '../models/city';

export const handler: Handler = (event: APIGatewayEvent, context: Context, callback: Callback) => {
	context.callbackWaitsForEmptyEventLoop = false;
	const {stateId} = event.pathParameters;
	const {name = ''} = event.queryStringParameters || {};
	const authorization = event.headers && event.headers.Authorization || '';

	validateAuthToken(authorization)
		.then(() => openDatabase())
		.then(() => getCities(stateId, name))
		.then(mapToResourceCities)
		.then(getSuccessLambdaResponse)
		.then((response: ILambdaResponse) => callback(null, response))
		.catch((error: IError) => {
			error = error.isHandled ? error : createRuntimeError(error.message);
			return createErrorLambdaResponse(error)
				.then((response: any) => callback(null, response));
		});
};


function getCities(stateId: string, name = ''): Promise<ICustomer[] | IError> {
	const filter = {
		stateId: stateId
	};

	if (name) {
		filter['name'] = { $regex: new RegExp(name, 'gi')  };
	}

	return new Promise((resolve, reject) => {
		return CityModel.find(filter, null, { sort: {name: 1} }, (error, data) => {
			if (error) {
				return reject(createDatabaseError(error.message));
			}
			return resolve(data);
		});
	});
}


