import {APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda';
import {CompanyModel} from '../models/company';
import {CORS_HEADERS, CONTENT_TYPE_JSON} from '../shared/constants';
import {openDatabase} from '../shared/database';
import {mapToResource} from '../shared/json-api';

const headers = {...CONTENT_TYPE_JSON, ...CORS_HEADERS};


export const handler: Handler = (event: APIGatewayEvent, context: Context, callback: Callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    const body = event.body ? JSON.parse(event.body) : event;

    processEvent(body)
        .then((response: any) => mapToResource('companies', response))
        .then((resource: any) => {
            callback(null, {
                statusCode: 201,
                headers: CORS_HEADERS,
                body: JSON.stringify(resource)
            });
        })
        .catch((error) => {
            console.log('There is an error processing the operation');
            callback(null, {
                statusCode: 400,
                headers: CORS_HEADERS,
                body: JSON.stringify(error)
            });
        });
};

function processEvent(event: APIGatewayEvent): Promise<any> {
    return createCompany(event);
}

async function createCompany(event: APIGatewayEvent): Promise<any> {
    try {
        await openDatabase();

        const company = new CompanyModel(event);
        const validationError = company.validateSync();

        if (validationError) {
            return Promise.reject(validationError);
        }

        const resource = company.save();
        return Promise.resolve(resource);
    } catch (error) {
        return Promise.reject(error);
    }
}


