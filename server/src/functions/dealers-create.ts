import {APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda';
import {DealerModel} from '../models';
import {openDatabase} from '../shared/database';
import {mapToResourceDealer} from '../shared/json-api';
import {IDealer, IError} from '../../src-shared/interfaces';
import {
	createErrorLambdaResponse, getCreatedLambdaResponse, ILambdaResponse
} from '../shared/lambda-response';
import {createDatabaseError, createRuntimeError} from '../../src-shared/error.factory';
import {validateAuthToken} from '../shared/validate-token';
import {
	createUser as createCognitoUser,
	Groups
} from '../shared/cognito';

export const handler: Handler = (event: APIGatewayEvent, context: Context, callback: Callback) => {
	context.callbackWaitsForEmptyEventLoop = false;
	const user = JSON.parse(event.body);
	const authorization = event.headers && event.headers.Authorization || '';

	user.groups = [Groups.Dealers];
	validateAuthToken(authorization)
		.then(() => createCognitoUser(user))
		.then(() => openDatabase())
		.then(() => createDealer(user))
		.then(mapToResourceDealer)
		.then(getCreatedLambdaResponse)
		.then((response: ILambdaResponse) => callback(null, response))
		.catch((error) => {
			return createErrorLambdaResponse(error.isHandled ? error : createRuntimeError(error.message))
				.then((response: any) => callback(null, response));
		});
};

function createDealer(dealer: any): Promise<IDealer | IError> {
	return new Promise((resolve, reject) => {
		const newUser = new DealerModel(dealer);
		return newUser.save((error, result) => {
			if (error) {
				return reject(createDatabaseError(error.message));
			}
			return resolve(result);
		});
	});
}
