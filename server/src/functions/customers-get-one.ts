import {APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda';
import {CustomerModel} from '../models';
import {openDatabase} from '../shared/database';
import {mapToResourceCustomer} from '../shared/json-api';
import {IAddress, ICustomer, IError, IResourceCustomer} from '../../src-shared/interfaces';
import {
	createErrorLambdaResponse, getSuccessLambdaResponse, ILambdaResponse
} from '../shared/lambda-response';
import {validateAuthToken} from '../shared/validate-token';
import {
	createDatabaseError,
	createRuntimeError
} from '../../src-shared/error.factory';

export const handler: Handler = (event: APIGatewayEvent, context: Context, callback: Callback) => {
	context.callbackWaitsForEmptyEventLoop = false;
	const {customerId} = event.pathParameters;

	const authorization = event.headers && event.headers.Authorization || '';

	validateAuthToken(authorization)
		.then(() => openDatabase())
		.then(() => getCustomerById(customerId))
		.then(mapToResourceCustomer)
		.then(getSuccessLambdaResponse)
		.then((response: ILambdaResponse) => callback(null, response))
		.catch((error: IError) => {
			error = error.isHandled ? error : createRuntimeError(error.message);

			return createErrorLambdaResponse(error)
				.then((response: any) => callback(null, response));
		});
};

function getCustomerById(customerId: string): Promise<ICustomer | IError> {
	return new Promise((resolve, reject) => {
		return CustomerModel
			.findById(customerId, (error, data) => {
				if (error) {
					return reject(createDatabaseError(error.message));
				}
				return resolve(data);
			});
	});
}

