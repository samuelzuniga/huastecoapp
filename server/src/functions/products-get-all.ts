import {APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda';
import {CompanyModel} from '../models';
import {openDatabase} from '../shared/database';
import {mapToResourceProducts} from '../shared/json-api';
import {IProduct} from '../../src-shared/interfaces';
import {
	createErrorLambdaResponse,
	getSuccessLambdaResponse,
	ILambdaResponse
} from '../shared/lambda-response';
import {validateAuthToken} from '../shared/validate-token';
import {IError} from '../../src-shared/interfaces/error';

export const handler: Handler = (event: APIGatewayEvent, context: Context, callback: Callback) => {
	context.callbackWaitsForEmptyEventLoop = false;
	const {companyId} = event.pathParameters;

	const authorization = event.headers && event.headers.Authorization || '';

	validateAuthToken(authorization)
		.then((result: any) => getProducts(companyId))
		.then(mapToResourceProducts)
		.then(getSuccessLambdaResponse)
		.then((response: ILambdaResponse) => callback(null, response))
		.catch((error: IError) => {
			return createErrorLambdaResponse(error)
				.then((response: any) => callback(null, response));
		});
};

function getProducts(companyId: string): Promise<IProduct[] | IError> {
	return openDatabase()
		.then(() => CompanyModel.findById(companyId))
		.then((company) => Promise.resolve(company && company.products || []))
		.catch((error: IError) => Promise.reject(error));
}
