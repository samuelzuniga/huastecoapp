import {APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda';

import {mapToResourceUser} from '../shared/json-api';
import {createUser as createCognitoUser} from '../shared/cognito';
import {IUser} from '../../src-shared/interfaces';
import {CompanyModel} from '../models';
import {openDatabase} from '../shared/database';
import {getObjectWhitoutProperties} from '../shared/util-helper';
import {
	ILambdaResponse,
	getCreatedLambdaResponse,
	getBadRequestLambdaResponse,
	getUnauthorizedLambdaResponse,
} from '../shared/lambda-response';
import {validateAuthToken} from '../shared/validate-token';

export const handler: Handler = (event: APIGatewayEvent, context: Context, callback: Callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
	const {companyId} = event.pathParameters;
    const user: IUser = event.body ? JSON.parse(event.body) : event;
	const authorization = event.headers && event.headers.Authorization || '';

	validateAuthToken(authorization)
		.then(() => createUser(companyId, user))
		.then(mapToResourceUser)
		.then(getCreatedLambdaResponse)
		.then((response: ILambdaResponse) => callback(null, response))
		.catch((error) => {
			const lambdaResponse = (error && error.code && error.code === 'InvalidToken')
				?
					getUnauthorizedLambdaResponse
				:
					getBadRequestLambdaResponse;

			return lambdaResponse.bind(this)(error)
					.then((response: any) => callback(null, response));
		});
};

async function createUser(companyId: string, user: IUser): Promise<any> {
	try {
		await openDatabase();
		const cognitoUser = await createCognitoUser(user);
		const resource = await addUserToDatabase(companyId, user);

		if (resource && resource.users && resource.users.length) {
			const newUser =  resource.users.find((u: IUser) => u.username === user.username);
			if (newUser) {
				return Promise.resolve(cognitoUser);
			}
			return Promise.reject(new Error('Cannot create customer'));
		}

		return Promise.reject(new Error('Cannot create customer'));

	} catch (error) {
		return Promise.reject(new Error(error.message || error));
	}
}

function addUserToDatabase(companyId: string, user: IUser): Promise<any> {
	// we don't want to persist person info in database
	// this information is handled by cognito
	const newUser = getObjectWhitoutProperties(user, ['firstName', 'lastName', 'phoneNumber', 'email', 'groups', 'sub']);

	return CompanyModel.findByIdAndUpdate(companyId,
		{$push: {users: newUser}},
		{new: true, runValidators: true});
}
