import {APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda';
import {MongoClient} from 'mongodb';

import {CORS_HEADERS, CONTENT_TYPE_JSON} from '../shared/constants';

const headers = {...CONTENT_TYPE_JSON, ...CORS_HEADERS};
// https://docs.mongodb.com/manual/reference/connection-string/#examples

let dbConnectionUri = null;
let cachedDb = null;

export const handler: Handler = (event: APIGatewayEvent, context: Context, cb: Callback) => {
    // MONGODB_HUASTECO_CLUSTER_URI
    let uri = process.env['MONGODB_HUASTECO_CLUSTER_URI'];

    if (dbConnectionUri !== null) {
        processEvent(event, context, cb);
    } else {
        dbConnectionUri = uri;
        console.log('The Attlas connection string is ' + dbConnectionUri);
        processEvent(event, context, cb);
    }

    /*
    cb(null, {
        statusCode: 200,
        headers ,
        body: JSON.stringify({
            message: 'Go Serverless v1.0! Your function executed successfully!',
            input: event,
        })
    })
    */
};

function processEvent(event: APIGatewayEvent, context: Context, cb: Callback) {
    console.log('Calling MongoDB Atlas from AWS Lambda with event: ' + JSON.stringify(event));
    let jsonContents = JSON.parse(JSON.stringify(event));

    // date conversion for grades array
    if (jsonContents.grades != null) {
        for (let i = 0, len = jsonContents.grades.length; i < len; i++) {
            // use the following line if you want to preserve the original dates
            // jsonContents.grades[i].date = new Date(jsonContents.grades[i].date);

            // the following line assigns the current date so we can more easily differentiate between similar records
            jsonContents.grades[i].date = new Date();
        }
    }

    // the following line is critical for performance reasons to allow re-use of database connections across calls to this Lambda function and avoid closing the database connection. The first call to this lambda function takes about 5 seconds to complete, while subsequent, close calls will only take a few hundred milliseconds.
    context.callbackWaitsForEmptyEventLoop = false;

    try {
        if (cachedDb == null) {
            console.log('=> connecting to database');
            MongoClient.connect(dbConnectionUri, function (err, client) {
                cachedDb = client.db('travel');
                return createDoc(cachedDb, jsonContents, cb);
            });
        }
        else {
            createDoc(cachedDb, jsonContents, cb);
        }
    } catch (err) {
        console.error('an error occurred', err);
    }
}

function createDoc(db, json, callback) {
    db.collection('restaurants').insertOne(json, function (err, result) {
        if (err != null) {
            console.error('an error occurred in createDoc', err);
            callback(null, JSON.stringify(err));
        }
        else {
            console.log('Kudos! You just created an entry into the restaurants collection with id: ' + result.insertedId);
            callback(null, 'SUCCESS');
        }
        // we don't need to close the connection thanks to context.callbackWaitsForEmptyEventLoop = false (above)
        // this will let our function re-use the connection on the next called (if it can re-use the same Lambda container)
        // db.close();
    });
};
