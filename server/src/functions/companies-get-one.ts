import {APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda';
import {CompanyModel} from '../models';
import {openDatabase} from '../shared/database';
import {mapToResourceCompany} from '../shared/json-api';
import {IError} from '../../src-shared/interfaces/error';
import {ICompany} from '../../src-shared/interfaces/company';
import {createErrorLambdaResponse, getSuccessLambdaResponse, ILambdaResponse} from '../shared/lambda-response';
import {validateAuthToken} from '../shared/validate-token';


export const handler: Handler = (event: APIGatewayEvent, context: Context, callback: Callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    const {companyId} = event.pathParameters;
	const authorization = event.headers && event.headers.Authorization || '';

	validateAuthToken(authorization)
		.then(() => getCompany(companyId))
		.then(mapToResourceCompany)
		.then(getSuccessLambdaResponse)
		.then((response: ILambdaResponse) => callback(null, response))
		.catch((error: IError) => {
			return createErrorLambdaResponse(error)
				.then((response: any) => callback(null, response));
		});
};

function getCompany(companyId: string): Promise<ICompany | IError> {
	return openDatabase()
		.then(() => CompanyModel.findById(companyId))
		.then((company: any) => {
			if (company) {
				return {
					id: company['_id'],
					name: company.name,
					description: company.description,
					address: company.address
				};
			}
			return company;
		})
		.then((company) => Promise.resolve(company))
		.catch((error: IError) => Promise.reject(error));
}

