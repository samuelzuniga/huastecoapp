import {APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda';
import {
	createErrorLambdaResponse,
	getSuccessLambdaResponse,
	ILambdaResponse
} from '../shared/lambda-response';
import {mapToResourceGroups} from '../shared/json-api';
import {validateAuthToken} from '../shared/validate-token';
import {listGroups} from '../shared/cognito';
import {IError} from '../../src-shared/interfaces/error';

export const handler: Handler = (event: APIGatewayEvent, context: Context, callback: Callback) => {
	context.callbackWaitsForEmptyEventLoop = false;
	const authorization = event.headers && event.headers.Authorization || '';

	validateAuthToken(authorization)
		.then((result: any) => listGroups())
		.then(mapToResourceGroups)
		.then(getSuccessLambdaResponse)
		.then((response: ILambdaResponse) => callback(null, response))
		.catch((error: IError) => {
			return createErrorLambdaResponse(error)
				.then((response: any) => callback(null, response));
		});
};




