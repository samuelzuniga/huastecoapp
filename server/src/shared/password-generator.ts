const PasswordGenerator = require('strict-password-generator').default;

const passwordGenerator = new PasswordGenerator();

const options = {
    upperCaseAlpha: false,
    lowerCaseAlpha: true,
    number: true,
    specialCharacter: true,
    minimumLength: 8,
    maximumLength: 8
};


export function generatePassword() {
    return passwordGenerator.generatePassword(options);
}
