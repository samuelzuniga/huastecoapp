
export function getObjectWhitoutProperties(obj: any, properties: string[] = []): any {
	const result = {...obj} ;

	properties.forEach((property: string) => {
		if (result.hasOwnProperty(property)) {
			delete result[property];
		}
	});

	return result;

}