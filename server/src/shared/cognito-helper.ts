import {CognitoUserSession} from 'amazon-cognito-identity-js';

import {
	IUserAuthenticated,
	IUser,
	INameValue,
	IUserTokens
} from '../../src-shared/interfaces';

export function mapUserAttributesArrayToObject(attributes: INameValue[]): any {
	console.log('ATTRIBUTES ARE');
	console.log(attributes);
	console.log('----');

	const mapFields = {
		'email': 'email',
		'phone_number': 'phoneNumber',
		'custom:firstname': 'firstName',
		'custom:lastname' : 'lastName',
		'sub': 'id'
	};

	const o = {};

	attributes.forEach((attribute: INameValue) => {
		o[mapFields[attribute.Name]
			?
				mapFields[attribute.Name]
			:
				attribute.Name] = attribute.Value;
	});

	return o;
}

export function getEmptyUser(): IUser {
	return  {
		username: null,
		email: null,
		firstName: null,
		lastName: null,
		phoneNumber: null,
		groups: [],
		id: null,
		pictureURL: ''
	};
}

export function getEmptyUserAuthenticated(): IUserAuthenticated {
	return {
		...getEmptyUser(),
		...{
			isAuthenticated: false,
			accessToken: null,
			idToken: null,
			refreshToken: null,
			company: null
		}
	};
}

export function getAuthorizationTokens(session: CognitoUserSession): IUserTokens {
	return {
		accessToken: session.getAccessToken().getJwtToken(),
		idToken: session.getIdToken().getJwtToken(),
		refreshToken: session.getRefreshToken().getToken()
	};
}
