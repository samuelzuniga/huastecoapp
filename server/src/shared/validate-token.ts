const jose = require('node-jose');
import {createInvalidTokenError} from '../../src-shared/error.factory';

const appClientId =  process.env.HUASTECO_CLIENT_ID;

// update environment variables when it requires it
const jwks =  process.env.HUASTECO_JWKS;

export function validateAuthToken(token: string = ''): Promise<any> {
	return new Promise((resolve, reject) => {
		if (!token) {
			return reject(createInvalidTokenError('The Authorization header was not provided'));
		}
		let keys = null;
		let key_index = -1;

		try {
			const sections = token.split('.');

			// get the kid from the headers prior to verification
			let header = jose.util.base64url.decode(sections[0]);
			header = JSON.parse(header);
			const kid = header.kid;

			keys = JSON.parse(jwks)['keys'];
			// search for the kid in the downloaded public keys
			for (let i = 0; i < keys.length; i++) {
				if (kid === keys[i].kid) {
					key_index = i;
					break;
				}
			}
			if (key_index === -1) {
				return reject(createInvalidTokenError('Public key not found in jwks.json'));
			}
		} catch (error) {
			console.log(error);
			return reject(createInvalidTokenError('The token provided is invalid'));
		}


		// construct the public key
		jose.JWK.asKey(keys[key_index]).
		then(function(result) {
			// verify the signature
			jose.JWS.createVerify(result).
			verify(token).
			then(function(verifyResult) {
				// now we can use the claims
				const claims = JSON.parse(verifyResult.payload);
				// additionally we can verify the token expiration
				const current_ts = Math.floor(new Date() / 1000);
				if (current_ts > claims.exp) {
					return reject(createInvalidTokenError('Token is expired'));
				}
				// and the Audience (use claims.client_id if verifying an access token)
				if (claims.aud !== appClientId) {
					return reject(createInvalidTokenError('Token was not issued for this audience'));
				}
				return resolve(claims);
			}).
			catch(function() {
				return reject(createInvalidTokenError('Signature verification failed'));
			});
		});
	});
}
