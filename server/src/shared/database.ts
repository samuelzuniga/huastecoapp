import * as mongoose from 'mongoose';
import {createDatabaseError} from '../../src-shared/error.factory';

let isConnectionOpen = false;

export function openDatabase(): Promise<any> {
	const dbConnectionString = process.env['MONGODB_HUASTECO_CLUSTER_URI'];

	return new Promise((resolve, reject) => {
		if (!isConnectionOpen) {
			return mongoose.connect(dbConnectionString, (error) => {
				if (error) {
					return reject(createDatabaseError('Error connecting database'));
				}
				isConnectionOpen = true;
				return resolve(this);
			});
		}
		return resolve(this);
	});

}
