import {
	IResourceCustomer,
	IResourceProduct,
	ICustomer,
	IError,
	IResourceUserAuthenticated,
	IUserAuthenticated,
	IProduct,
	ICompany,
	IDistrict,
	IGroup,
	IResourceGroup,
	IUser,
	IDealer,
	IResourceUser,
	IResourceDealer,
	IResourceProducts,
	IResourceCompany,
	IState,
	IResourceStates,
	IResourceCustomers,
	IResourceDistricts
} from '../../src-shared/interfaces';
import {createInvalidParameterError} from '../../src-shared/error.factory';

export function mapToResourceUserAuthenticated(resource: IUserAuthenticated): Promise<IResourceUserAuthenticated> {
	return mapToResource('users', resource, 'id');
}

export function mapToResourceCustomer(resource: ICustomer): Promise<IResourceCustomer> {
	return mapToResource('customers', resource);
}

export function mapToResourceUser(resource: IUser): Promise<IResourceUser> {
	return mapToResource('users', resource, 'id');
}

export function mapToResourceDealer(resource: IDealer): Promise<IResourceDealer> {
	return mapToResource('dealers', resource, 'id');
}

export function mapToResourceProduct(resource: IProduct | IProduct[]): Promise<IResourceProduct> {
	return mapToResource('products', resource);
}

export function mapToResourceCompany(resource: ICompany): Promise<IResourceCompany> {
	return mapToResource('companies', resource, 'id');
}

export function mapToResourceProducts(resource: IProduct[]): Promise<IResourceProducts> {
	return mapToResources('products', resource);
}

export function mapToResourceCustomers(resource: ICustomer[]): Promise<IResourceCustomers> {
	return mapToResources('customers', resource);
}

export function mapToResourceStates(resource: IState[]): Promise<IResourceStates> {
	return mapToResources('states', resource);
}

export function mapToResourceCities(resource: ICustomer[]): Promise<IResourceCustomers> {
	return mapToResources('cities', resource);
}

export function mapToResourceDistricts(resource: IDistrict[]): Promise<IResourceDistricts> {
	return mapToResources('districts', resource);
}

export function mapToResourceGroups(resource: IGroup[]): Promise<IResourceGroup> {
	return mapToResources('groups', resource, 'id');
}

// todo generics
export function mapToResource(entityName: string, resource: any | any[], id = '_id'): Promise<any> {
	return Promise.resolve(
    {
			data: {
				type: entityName,
				id: resource[id],
				attributes: resource
			},
			links: {
				self: `/${entityName}/${resource[id]}`
			}
		}
	);
}

export function mapToResources<T>(entityName: string, resource: T[], id = '_id'): Promise<IError | any> {
	return new Promise((resolve, reject) => {
		if (!Array.isArray(resource)) {
			return reject(
				createInvalidParameterError('function:mapToResource, param resource should be an array')
			);
		}

		const data = resource.map((r) => {
			return {
				type: entityName,
				id: r[id],
				attributes: r
			};
		});

		return resolve({ data } );
	});


}


