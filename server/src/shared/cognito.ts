import * as AWS from 'aws-sdk';
import {CognitoUser, AuthenticationDetails, UserData, CognitoUserSession} from 'amazon-cognito-identity-js';
const AmazonCognitoIdentity = require('amazon-cognito-identity-js');

import {
	IPasswordChange,
	IUserPool
} from '../../src-shared/interfaces';

// "us-east-1"
import {generatePassword} from './password-generator';
import {
    IUser,
	IUserAuthenticated,
	IUserCredentials
} from '../../src-shared/interfaces';
import {
	getEmptyUser,
	getEmptyUserAuthenticated,
	getAuthorizationTokens,
	mapUserAttributesArrayToObject
} from './cognito-helper';
import {IError} from '../../src-shared/interfaces/error';
import {createCognitoError} from '../../src-shared/error.factory';


const USER_POOL = {
	userPoolId: process.env.HUASTECO_USER_POOL_ID,
	region: process.env.HUASTECO_REGION,
	clientId: process.env.HUASTECO_CLIENT_ID
};

export const Groups = {
	Dealers: 'Dealers',
	Operators: 'Operators',
	Administrators: 'Administrators'
};

export function forceChangePassword(credentials: IPasswordChange, userPool: IUserPool = null): Promise<IUserAuthenticated> {
    return new Promise((resolve, reject) => {
        const authenticationDetails = createAuthenticationDetails(credentials);
        const cognitoUser = createCognitoUser(credentials.username, userPool);

        return cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function (session: CognitoUserSession) {
				return buildAuthenticatedUser(cognitoUser, session)
					.then((user: IUserAuthenticated) => resolve(user))
					.catch((err) => reject(err));
            },
            onFailure: function (err) {
                return reject(err);
            },
            newPasswordRequired: (userAttributes, requiredAttributes) => {
                if (credentials.newPassword) {
                    const attributes = {};
                    cognitoUser.completeNewPasswordChallenge(credentials.newPassword, attributes, {
                        onSuccess: function (session: CognitoUserSession) {
							return buildAuthenticatedUser(cognitoUser, session)
								.then((user: IUserAuthenticated) => resolve(user))
								.catch((err) => reject(err));
                        },
                        onFailure: function (err) {
                            return reject(err);
                        }
                    });
                }
            }
        });
    });
}

export function listGroups(userPool: IUserPool = USER_POOL): Promise<any> {
	const params = {
		UserPoolId: userPool.userPoolId
	};

	const cognitoIdentityServiceProvider = new AWS.CognitoIdentityServiceProvider({region: userPool.region});

	return new Promise((resolve, reject) =>  {

		return cognitoIdentityServiceProvider.listGroups(params, (err, data) => {
			if (err) {
				return reject(createCognitoError(err.message || 'Error getting list groups'));
			}
			const groups = (data.Groups || [])
				.map((group) => ({id: group.GroupName, name: group.GroupName}));
			return resolve(groups);
		});
	});
}

export function createUser(user: IUser, userPool: IUserPool = null): Promise<IUser> {
	// todo validate person who is calling this service belongs to administrators or operators
	const pool = userPool || USER_POOL;

    const params = {
        UserPoolId: pool.userPoolId,
        Username: user.username,
        DesiredDeliveryMediums: ['SMS', 'EMAIL'],
        TemporaryPassword: generatePassword(),
        UserAttributes: [
            {
                Name: 'email',
                Value: user.email
            },
            {
                Name: 'phone_number',
                Value: user.phoneNumber
            },
            {
                Name: 'custom:firstname',
                Value: user.firstName
            },
            {
                Name: 'custom:lastname',
                Value: user.lastName
            },
            {
                Name: 'custom:role',
                Value: ''  // user starts without role
            }
        ]
    };

    const cognitoIdentityServiceProvider = new AWS.CognitoIdentityServiceProvider({region: pool.region});
    return new Promise((resolve, reject) => {
        return cognitoIdentityServiceProvider.adminCreateUser(params, function (errorCreateUser, userData) {
            if (errorCreateUser) {
            	return reject(createCognitoError(errorCreateUser.message || 'Error creating user'));
            }

            // todo make possible creating user add to some groups
			// right now only add the first group
            const groupParams = {
				'GroupName': user.groups[0],
				'Username': user.username,
				'UserPoolId': pool.userPoolId
			};

            cognitoIdentityServiceProvider.adminAddUserToGroup(groupParams, (errorAddGroup, data) => {
				if (errorAddGroup) {
					return reject(createCognitoError(errorAddGroup.message || 'Error adding user to group'));
				}

				const newUser = getEmptyUser();
				newUser.username = userData.User.Username;
				newUser.groups = user.groups;
				const {firstName, lastName, phoneNumber, id, email} = mapUserAttributesArrayToObject(userData.User.Attributes);
				newUser.firstName = firstName;
				newUser.lastName = lastName;
				newUser.phoneNumber = phoneNumber;
				newUser.id = id;
				newUser.email = email;

				return resolve(newUser);
			});


        });
    });
}

export function login(credentials: IUserCredentials, userPool: IUserPool = null): Promise<IUserAuthenticated> {
	return new Promise((resolve, reject) => {
		const authenticationDetails = createAuthenticationDetails(credentials);
		const cognitoUser = createCognitoUser(credentials.username, userPool);

		return cognitoUser.authenticateUser(authenticationDetails, {
			onSuccess: function (session: CognitoUserSession) {
				return buildAuthenticatedUser(cognitoUser, session)
					.then((user: IUserAuthenticated) => resolve(user))
					.catch((err) => reject(createCognitoError(err.message || 'Error authenticating user')));
			},
			onFailure: function (err) {
				return reject(createCognitoError(err.message || 'Failure authenticating user'));
			}
		});
	});
}


// NOT EXPORTABLE
function getUserData(cognitoUser: CognitoUser): any {
	return new Promise((resolve, reject) => {
		return cognitoUser.getUserData((error: Error, userData: UserData) => {
			if (error) { return reject(createCognitoError(error.message || 'Error getting cognito user data')); }

			return resolve(userData);
		});
	});
}

function buildAuthenticatedUser(cognitoUser: CognitoUser, session: CognitoUserSession): Promise<IUserAuthenticated> {
	return getUserData(cognitoUser)
		.then((userData) => {
			const user = getEmptyUserAuthenticated();
			user.username = userData.Username;
			user.isAuthenticated = true;
			user.groups = session['idToken'].payload['cognito:groups'] || [];
			const {firstName, lastName, phoneNumber, email, id} = mapUserAttributesArrayToObject(userData.UserAttributes);
			user.email = email;
			user.id = id;
			user.firstName = firstName;
			user.lastName = lastName;
			user.phoneNumber = phoneNumber;

			return ({
				...user,
				...getAuthorizationTokens(session)
			});
		});
}

function createCognitoUser(username: string, userPool: IUserPool = null): CognitoUser {
	const pool = userPool || USER_POOL;

	return  new CognitoUser({
		Username: username,
		Pool: createUserPool(pool)
	});
}

function createAuthenticationDetails(credentials: IUserCredentials): any  {
	return new AuthenticationDetails({
		Username: credentials.username,
		Password: credentials.password
	});
}

function createUserPool(userPool: IUserPool): any {
	const poolData = {
		UserPoolId: userPool.userPoolId, // Your user pool id here
		ClientId: userPool.clientId // Your client id here
	};

	return new AmazonCognitoIdentity.CognitoUserPool(poolData);
}
