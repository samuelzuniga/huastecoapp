export const CORS_HEADERS = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Credentials': true
};

export const CONTENT_TYPE_JSON = {'Content-Type': 'application/json'};
