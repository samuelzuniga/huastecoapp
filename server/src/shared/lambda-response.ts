import {CORS_HEADERS, CONTENT_TYPE_JSON} from '../shared/constants';
import {IError} from '../../src-shared/interfaces/error';

export interface ILambdaResponse {
	statusCode: number | string,
	headers: any;
	body: string;
}

export function createErrorLambdaResponse(error: IError): Promise<ILambdaResponse> {
	return getErrorLambdaResponse(error);
}

export function getSuccessLambdaResponse(resource: any): Promise<ILambdaResponse> {
	return getLambdaResponse(200, resource);
}

export function getUnauthorizedLambdaResponse(resource: any): Promise<ILambdaResponse> {
	return getLambdaResponse(401, resource);
}

export function getCreatedLambdaResponse(resource: any): Promise<ILambdaResponse> {
	return getLambdaResponse(201, resource);
}

export function getBadRequestLambdaResponse(resource: any): Promise<ILambdaResponse> {
	return getLambdaResponse(400, resource);
}

function getErrorLambdaResponse(error: IError): Promise<ILambdaResponse> {
	const headers = {...CONTENT_TYPE_JSON, ...CORS_HEADERS};

	return Promise.resolve(
		{
			statusCode: error.code.toString(),
			headers,
			body: JSON.stringify(error)
		}
	);
}


function getLambdaResponse(statusCode: number, resource: any): Promise<ILambdaResponse> {
	const headers = {...CONTENT_TYPE_JSON, ...CORS_HEADERS};

	return Promise.resolve(
	{
		statusCode: statusCode,
		headers,
		body: JSON.stringify(resource)
		}
	);
}
