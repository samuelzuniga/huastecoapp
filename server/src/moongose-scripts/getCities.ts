import {openDatabase} from '../shared/database';
import {CityModel} from '../models/city';

const stateId = '5bc23deb69dd1a0acd2ba2e4';


openDatabase()
	.then(getCities)
	.then((results) => {
		console.log('Cities');
		console.log(results);
	});


function getCities(): Promise<any> {
	const reg = new RegExp('ti', 'gi');

	const filter = {
		stateId: stateId // ,
		// name: { $regex: reg }  // { $regex: /ti/i }
	};

	return new Promise((resolve, reject) => {
		return CityModel.find(filter, null, { sort: {name: 1}}, (error, data) => {
			if (error) {
				return reject(error);
			}
			return resolve(data);
		});
	});
}
