import {openDatabase} from '../shared/database';
import {CustomerModel} from '../models';

const companyId = '5b9560282a14070a774a4faf';


openDatabase()
	.then(getCustomers)
	.then((results) => {
		console.log('Customers');
		console.log(results);
	});


function getCustomers(): Promise<any> {
	const reg = new RegExp('2345', 'gi');

	const filter = {
		companyId: companyId,
		phoneNumber: { $regex: reg }
	};

	const projection = {
		_id: 1,
		firstName: 1,
		lastName: 1,
		phoneNumber: 1,
		fullName: 1
	};

	return new Promise((resolve, reject) => {
		return CustomerModel
			.find(filter, projection, { sort: {name: 1}}, (error, data) => {
				if (error) {
					return reject(error);
				}
				return resolve(data);
			});
	});
}
