import {createDatabaseError} from '../src-shared/error.factory';

const csvdata = require('csvdata');
import {openDatabase} from '../src/shared/database';
import {IError} from '../src-shared/interfaces';
import {DistrictModel} from '../src/models/district';

const cityId = '5bc2402269dd1a0acd2ba2e7';

openDatabase()
	.then(function () {
        return csvdata.load('./input-file/data.csv')
            .then((records) =>  {
                return (records || [])
                    .map((record) =>
						({ cityId: cityId, name: record['Asentamiento'], postalCode: record['Código Postal'] }));
            });
	})
	// .then((results) => console.log(results))
	.then(importDistricts)
	.then(() => console.log('OK'))
	.catch((error) => console.log('Error:', error));


function importDistricts(districts: any[]): Promise<any| IError> {
	return new Promise((resolve, reject) => {
		return DistrictModel.create(districts, (error, result) => {
			if (error) {
				return reject(createDatabaseError(error.message));
			}
			return resolve(result);
		});

	});
}
