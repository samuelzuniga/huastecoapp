import {IError} from './interfaces';

const UNKNOWN_ERROR = 'Unknown error';


export enum CodeErrors {
	BAD_REQUEST = 400,
	UNAUTHORIZED = 401,
	FORBIDDEN = 403,
	NOT_FOUND = 404,
	SERVER_ERROR = 500
}

export const ErrorNames  = {
	MissingParameters: 'HuastecoMissingParameters',
	InvalidToken: 'HuastecoInvalidToken',
	DatabaseError: 'HuastecoDatabaseError',
	CognitoError: 'HuastecoCognitoError',
	InvalidParameter: 'HuastecoInvalidParameter',
	InvalidCredentials: 'HuastecoInvalidCredentials',
	RuntimeError: 'HuastecoRuntimeError'
};

export function createMissingParametersError(message = UNKNOWN_ERROR): IError {
	return {
		code: CodeErrors.BAD_REQUEST,
		name: ErrorNames.MissingParameters,
		isHandled: true,
		message
	};
}

export function createInvalidCredentials(message = UNKNOWN_ERROR): IError {
	return {
		code: CodeErrors.UNAUTHORIZED,
		name: ErrorNames.InvalidCredentials,
		isHandled: true,
		message
	};
}

export function createInvalidParameterError(message = UNKNOWN_ERROR): IError {
	return {
		code: CodeErrors.BAD_REQUEST,
		name: ErrorNames.InvalidParameter,
		isHandled: true,
		message
	};
}

export function createInvalidTokenError(message = UNKNOWN_ERROR): IError {
	return {
		code: CodeErrors.BAD_REQUEST,
		name: ErrorNames.InvalidToken,
		isHandled: true,
		message
	};
}

export function createDatabaseError(message = UNKNOWN_ERROR): IError {
	return {
		code: CodeErrors.SERVER_ERROR,
		name: ErrorNames.DatabaseError,
		isHandled: true,
		message
	};
}

export function createCognitoError(message = UNKNOWN_ERROR): IError {
	return {
		code: CodeErrors.SERVER_ERROR,
		name: ErrorNames.CognitoError,
		isHandled: true,
		message
	};
}

export function createRuntimeError(message = UNKNOWN_ERROR): IError {
	return {
		code: CodeErrors.SERVER_ERROR,
		name: ErrorNames.RuntimeError,
		isHandled: true,
		message
	};
}

