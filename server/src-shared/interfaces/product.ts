export interface IProduct {
	id?: string;
	name: string;
	brand: string;
	unitaryPrice: number;
}

export interface IGroup {
	id?: string;
	name: string;
}
