export interface IUserAttributes {
	Username: string;
	UserAttributes: [{Name: string, Value: string}];
}

export interface INameValue {
	Name: string;
	Value?: any;
}
