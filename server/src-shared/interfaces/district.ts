export interface IDistrict {
	id: string;
	cityId: string;
	name: string;
	postalCode: number;
	created: any;
}