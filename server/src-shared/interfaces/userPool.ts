export interface IUserPool {
    userPoolId: string;
    clientId: string;
    region: string;
}