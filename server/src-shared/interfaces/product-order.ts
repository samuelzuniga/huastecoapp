import {IProduct} from './product';

export interface IProductOrder {
	product: IProduct,
	quantity: number;
	date?: any;
	subtotal?: number;
}