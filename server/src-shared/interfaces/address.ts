import {ICoordinates} from './coordinates';

export interface IAddress {
	id?: string;
	street: string;
	number: string;
	insideNumber?: string;
	suburb: string;
	locality: string;
	postalCode: number;
	state: string;
	country: string;
	coordinates: ICoordinates;
}