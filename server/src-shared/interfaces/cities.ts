export interface ICity {
	id: string;
	stateId: string;
	name: string;
	longName: string;
}