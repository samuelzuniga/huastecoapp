export interface IGeoCoderRequest {
	address: string;
	locality: string;
	postalCode?: number;
	country: string;
}