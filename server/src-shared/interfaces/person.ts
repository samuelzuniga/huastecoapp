export interface IPerson {
	id?: string;
	firstName: string;
	lastName: string;
	phoneNumber: string;
	pictureURL: string;
	fullName?: string;
}