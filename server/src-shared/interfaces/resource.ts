import {
	IUser,
	ICustomer,
	IProduct,
	IGroup,
	IUserAuthenticated
} from './index';
import {ICompany} from './company';
import {IDealer} from './dealer';
import {IState} from './state';
import {IDistrict} from './district';
import {ICity} from './cities';

export interface IData<T> {
	type: string;
	id: string;
	attributes: T;
}

export interface IResource<T> {
	data: IData<T>;
	links: {
		self: string;
	};
}

export interface IResourceUserAuthenticated extends IResource<IUserAuthenticated> {}
export interface IResourceUser extends IResource<IUser> {}
export interface IResourceDealer extends IResource<IDealer> {}
export interface IResourceCustomer extends IResource<ICustomer> {}
export interface IResourceGroup extends IResource<IProduct> {}
export interface IResourceProduct extends IResource<IGroup> {}
export interface IResourceCompany extends IResource<ICompany> {}


export interface IResourceStates {
	data: IData<IState>[];
}

export interface IResourceCustomers {
	data: IData<ICustomer>[];
}

export interface IResourceCities {
	data: IData<ICity>[];
}

export interface IResourceDistricts {
	data: IData<IDistrict>[];
}

export interface IResourceCustomers {
	data: IData<ICustomer>[];
}

export interface IResourceProducts {
	data: IData<IProduct>[];
}

export interface IResourceGroups {
	data: IData<IGroup>[];
}

