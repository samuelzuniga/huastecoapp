import {IPerson, IAddress} from './index';

export interface ICustomer extends IPerson {
	id?: string;
	companyId: string | number;
	addresses: IAddress[];
}