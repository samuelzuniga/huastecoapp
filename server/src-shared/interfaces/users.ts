import {IPerson} from './person';
import {ICompany} from './company';

export interface IUserCredentials {
	username: string;
	password: string;
}

export interface IPasswordChange extends IUserCredentials {
	newPassword: string;
}

export interface IUserTokens {
	accessToken: string;
	idToken: string;
	refreshToken: string;
}

export interface IUser extends IPerson {
	username: string;
	email: string;
	groups: string[];
	id: string;
}


export interface IUserAuthenticated extends IUser, IUserTokens {
	isAuthenticated: boolean;
	company?: ICompany;
}
