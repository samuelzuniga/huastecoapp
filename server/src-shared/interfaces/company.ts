import {IAddress} from './address';

export interface ICompany {
	id: string | number;
	name: string;
	description: string;
	phoneNumber: string;
	address: IAddress;
}