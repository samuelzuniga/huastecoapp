export interface IError {
	code: string | number;
	name: string;
	message: string;
	isHandled: boolean;
}
