import {IPerson} from './person';

export interface IDealer extends IPerson {
	companyId: string;
	email: string;
	username: string;
	pictureURL: string;
	created: string;
}
