export interface IState {
	id: string;
	abbreviation: string;
	name: string;
	longName: string;
}
